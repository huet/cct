(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Format;
open Util;
open Term;
open Expression;
open Lambda;
open Reduction;
open Camlp4.PreCast;
open Syntax;

(* Sets of positions as marked terms *)

(* We want to represent sets of redex positions in a term. We want these
sets to be structured by the prefix ordering of positions. This way
any two positions will share their common prefix. The most natural
solution is to represent positions as marks in the term itself.
Since in the following we shall care only about residuals of redexes, we
only need marks at the App nodes. These marked terms will represent one
step of parallel reduction of all the marked redexes, or orthogonally
the set of residuals of a set of redexes by some derivation. *)

type redexes =  (* terms with boolean marks at applications *)
  [ MRef of int                            (* x *)
  | MAbs of redexes                        (* [x]t *)
  | MApp of bool and redexes and redexes   (* (t u) or (#t u) *)
  ]
;
(* We define substitution just like for ordinary terms *)

(* Recomputes references of global variables across n levels of bindings *)
value mlift n = mlift_rec 0
where rec mlift_rec k = mlift_k
where rec mlift_k = fun
  [ MRef i          -> if i<k then MRef i else MRef (n+i)
  | MAbs lam        -> MAbs (mlift_rec (k+1) lam)
  | MApp b lam lam' -> MApp b (mlift_k lam) (mlift_k lam')
  ]
;
value msubst lam = subst_lam 0
where rec subst_lam n = subst_n
where rec subst_n = fun
  [ MRef(k) -> if k=n then mlift n lam 
               else if k<n then MRef k else MRef (k-1)
  | MAbs lam' -> MAbs (subst_lam (n+1) lam')
  | MApp b lam1 lam2 -> MApp b (subst_n lam1) (subst_n lam2)
  ]
;
(* Ex {x<-[z]z} [y](y x)  =>  [y](y [z]z) 
 (msubst (MAbs (MRef 0)) (MAbs (MApp False (MRef 0) (MRef 1))) = 
               (MAbs (MApp False (MRef 0) (MAbs (MRef 0))))); *)

(* The following just for testing *)

(* Concrete syntax: marked terms *)

type mconcrete =
  [ MVar of string                               (* x *)
  | MLambda of string and mconcrete              (* [x]t *)
  | MApply of bool and mconcrete and mconcrete   (* (t1 t2) *)
  ]
;
(***********)
(* parsing *)
(***********)

(* parsing marked expressions in an environment *)
value rec mparse_env lvars = abstract
where rec abstract = fun
 [ MVar name      -> try MRef (index_of name lvars)  (* Global(name) *) 
                     with [ Failure _ -> error "Expression not closed" ]
 | MLambda id c   -> MAbs (mparse_env [id::lvars] c)
 | MApply b c1 c2 -> MApp b (abstract c1) (abstract c2)
 ]
;
(* mparser : (concrete -> redexes)   parses closed marked terms *)
value mparse = mparse_env []
;

(*************)
(* Unparsing *)
(*************)

value mconcrete free_vars = 
  concrete_rec free_vars (List.length free_vars)
  where rec concrete_rec env depth = fun
  [ MRef n       -> MVar (if n>=depth then free_var (n-depth)
                                      else List.nth env n)
  | MAbs l       -> let x = bound_var depth in
                    MLambda x (concrete_rec [x::env] (depth+1) l)
  | MApp b l1 l2 -> MApply b (concrete_rec env depth l1)
                             (concrete_rec env depth l2)
  ]
;
(* unmparser : marked -> concrete *)
value unmparse = mconcrete []
;
(***********)
(* Grammar *)
(***********)

module Gram = MakeGram Lexer
;
value marked = Gram.Entry.mk "marks"
and   mnamed = Gram.Entry.mk "mnamed"
and  lmnamed = Gram.Entry.mk "lmnamed"
and  mbinder = Gram.Entry.mk "mbinder"
;
EXTEND Gram
  GLOBAL: marked mnamed lmnamed mbinder;
  marked:
    [ [ e = mnamed; `EOI -> mparse e ] ];
  mnamed:
    [ [ "["; b = mbinder; "]"; e = mnamed
        -> let func v e = MLambda v e 
           in List.fold_right func b e
      | "("; "#"; e = lmnamed; ")" -> match e with 
            [ MApply False x y -> MApply True x y 
            | _ -> fail () 
            ]
      | "("; e = lmnamed; ")" -> e
      | x = LIDENT -> MVar x
    ] ];
  lmnamed:
    [ [ e = mnamed -> e
      | l = lmnamed; e = mnamed -> MApply False l e
    ] ];
  mbinder:
    [ [ x = LIDENT -> [x]
      | x = LIDENT; ","; b = mbinder -> [x::b]
    ] ];
END
;
value mark = Gram.parse_string marked (Loc.mk "From string")
;
(***********)
(* Printer *)
(***********)

(* Prints bound variables as x1,x2,... and free variables as G1,G2,... *)
value print_marked l = 
let rec print_rec env depth = 
  let rec printer flag = fun
   [ MRef n  -> printf "%s" (if n>=depth then free_var (n-depth)
                             else List.nth env n)
   | MApp b l1 l2 -> do { if flag then printf "@[<1>(" else ()
                        ; if b then printf "#" else ()
                        ; printer False l1
                        ; printf " "
                        ; printer True l2
                        ; if flag then printf ")@]" else ()
                        }
   | abs -> let (names,body) = peel depth [] abs in
            do { print_binder (List.rev names)
               ; print_rec (names @ env) (depth+List.length names) body
               }
   ]
  and peel d names = fun
   [ MAbs l -> let depth = d+1 in
               let name = bound_var depth in
               peel depth [name::names] l
   | other  -> (names,other)
   ]
  and print_binder = fun
    [ [] -> ()
    | [x::rest] -> do
      { printf "@[<1>[%s" x
      ; List.iter (fun x -> printf ",%s" x) rest
      ; printf "]@]"
      }
    ] in
  printer True in
print_rec [] 0 l
;
(* #install_printer print_marked; *)

(* Reduce a marked to head normal form *)
value rec hmnf = fun
  [ MRef n         -> MRef n
  | MAbs lam       -> MAbs (hmnf lam)
  | MApp True (MAbs lam) lam2 -> hmnf(msubst lam2 lam)
  | MApp True _ _  -> error "Marked non-redex - illegal"
  | MApp False lam1 lam2 -> let lam'=hmnf lam1 in 
                            match lam' with
        [ MAbs lam -> hmnf(msubst lam2 lam)
        | _        -> MApp False lam' lam2
        ]
  ]
;
(* Normal-order normal form *)
value rec mnf = fun
  [ MRef n        -> MRef n
  | MAbs(lam)     -> MAbs (mnf lam)
  | MApp True (MAbs lam) lam2 -> hmnf(msubst lam2 lam) 
  | MApp True _ _ -> error "Marked non-redex - illegal"
  | MApp False lam1 lam2 -> let lam1'=hmnf lam1 in 
                            match lam1' with
        [ MAbs lam -> mnf(msubst lam2 lam)
        | _        -> MApp False (mnf lam1') (mnf lam2)
        ]
  ]
;







