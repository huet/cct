(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

(* To parse quotations in assertions *)
#load "./util.cmo";
#load "./term.cmo";
#load "./expression.cmo";
#load "./lambda.cmo";

open Util;
open Term;
open Expression;
open Lambda;
open Reduction;
open Eta;
open Examples;
open Arith;

(* We are now convinced of the correctness of the normal interpreter.
   However, it is not reasonable computationally to iterate the search
   for the left-most outermost redex repeatedly from the top of the term,
   since there is a sense in which outermost computations reach approximations
   which are invariant by further reduction. This intuition is captured
   by the notion of head normal form. *)

(* Head normal forms *)
(* A term t of the form [x1, ..., xn](x t1 ... tp) is called a head normal form.
   Note that if red t s, then s = [x1, ..., xn](x s1 ... sp).
   Thus n,x,and p are invariant by beta-reduction. 

Remark. Every term is either a head normal form, or of the form:
 [x1, ..., xn](r t1 ... tp), with r a redex. In the second case,
 r is called the head redex  of the term. *)

type head = [ Head of (int * int * list term) ];

(* Now we can reduce a term to its head normal form, when it has one,
   by the normal reduction. Compare with algorithm hnf above. *)

(* head_normal : term -> head *)
value head_normal = head 0 []
  where rec head n args = fun
  [ Ref x   -> Head(n,x,args)
  | Abs t   -> match args with
                [ []     -> head (n+1) [] t
                | [a::r] -> head n r (subst a t)
                ]
  | App f x -> head n [x::args] f
  ]
;
(* We say that a term t is defined iff head_normal(t) terminates.
Proposition. If (f x) is defined, so is f. If [x]t is defined, so is t. *)

(* Proposition. If M is defined, the normal reduction issued from t has an
initial sequence which reduces t to s=(hnf t), and s is the first head 
normal form in the reduction sequence. *)


(******************)
(* Solvable terms *)
(******************)

(* We write below |M| for closure(M).

Definition. A term t is said to be solvable iff there exist n terms
a1 a2 ... an such that conv (|t| a1 ... an) I.

Theorem of Wadsworth. A term is defined iff it is solvable.

Proof. Let t be a term, which we may assumed to be closed.
If it is solvable, it is defined, by standardisation and the proposition 
above. Conversely, let us assume t red [x1, ..., xn](x t1 ... tp).
Taking K_ p = [x, y1, ..., yp]x, and KpI = K_ p _I, we get:
(t KpI_1 ... KpI_n) red (Kp_I t1 ... tp) red I. *)

(* A normal interpreter *)
(* We may now organise the normal interpreter into iterating head: *)

(* normal forms *)
type normal = [ Normal of (int * int * list normal) ]
;
(* norm : term -> normal *)
value rec norm t = 
  match head_normal t
  with [ Head(n,x,args) -> Normal(n,x,List.map norm args) ]
;
(* Discussion. Now norm (or equivalently nf as a function mapping 
terms to terms) looks for redexes in a top-down manner, but without
exploring the initial part of the term which is formed by layers of head
normal forms, and which is invariant. But these procedures will loop on
terms without normal forms. 

We shall now see how to compute progressively
successive approximations of a term, in a potentially infinite structure
called the Bohm tree of the term. Each node in the tree corresponds
to one layer of head normal form. Undefined subterms are Undef leaves.
The Bohm tree is the generalisation of normal forms to infinite trees. *)


(**************)
(* Bohm trees *)
(**************)

(* A Bohm tree is a possibly infinite tree representing the limit of all
   computations issued from a given term. It consists of layers of
   approximations, each approximation corresponding to a head-normal form. *)

(* First of all we shall profit of the additional structure of head normal 
   forms to represent variables in a better way. Every variable is represented
   by a double key Index(k,i) where k addresses upwards the hnf layer
   where the variable is declared, while i indexes in the corresponding
   list of bound variables the variable in a left to right fashion.
   Thus in [x1,x2,...,xn]xi(...) the head variable is represented as
   Index(0,i). Note that this representation of head variables is invariant
   by head eta expansion. *)

(* Two-level variables *)
type var = [ Index of (int * int) ]
;
type bohm = [ Hnf of (int * var * list bohm)
            | Future of term
            ]
;
(* There are two sorts of nodes in a (partial) Bohm tree: Hnf nodes, where
   one hnf approximation has been computed, and Future nodes, containing a
   lambda term waiting to be examined. When this term is undefined the tree
   cannot be grown further in this direction. Thus Future(t) is a sort of
   syntactic "bottom" meaning "not yet defined". *)

(* We now compute one level of approximation, evaluating a (defined) term
   into an Hnf form. The extra argument display contains a stack of natural
   numbers intended to hold the successive arities along a bohm tree branch *)
(* The auxiliary function lookup translates a Ref index into a display Index *)

value future t = Future t
;
value lookup m =  look(0,m) 
  where rec look(k,j) = fun
  [ [] -> error "Variable not in scope of display"
  | [n::rest] -> if j>=n then look (k+1,j-n) rest
                 else Index(k,n-j)
  ]
;
(* approx : list int -> term -> bohm *)
value approx display t = apx (0,[]) t
  where rec apx (n,args) = fun
  [ Ref m   -> Hnf(n,lookup m [n::display],List.map future args)
  | Abs t   -> match args with
                 [ []        -> apx (n+1,[]) t
                 | [a::rest] -> apx (n,rest) (subst a t)
                 ]
  | App f x -> apx (n,[x::args]) f
  ]
;
(* approximate : for closed terms *)
value approximate = approx [];

(* Remark that approximate(t) terminates iff t is defined. *)

(* evaluate a Bohm tree to Hnf *)
value evaluate display = fun
  [ Future bt -> approx display bt
  | hnf -> hnf
  ]
;
(****************)
(* Separability *)
(****************)


(* Definition. We say that terms t and s are separable iff there
exist closed terms a1, ..., an such that red (|t| a1 ... an) _True
and  red (|s| a1 ... an) _False.


Bohm theorem. Any two normal forms which are not eta convertible are separable.

We shall now develop additional notions which are needed for the proof of 
Bohm's theorem.

Definition. A path of length s is a sequence of positive integers:
p = Path[k1; ... ks]. *)

type path = [ Path of list int ]
;
(* A path is a position in a Bohm tree. At depth i, it indicates that
we access its ki's son, i.e. (nth bi ki-1), or if this is not possible that 
we effect the needed eta-expansions.

Let bt be an Bohm tree, pa a path.
We say that pa is accessible in bt towards bt' modulo eta iff
(bt',b,stack)=(access_tree ([],0) [] bt pa). 
The function access_tree collects 
occurrences of the first bound variable x along the path, with its arity p, 
in argument stack:arities. 
The boolean b indicates whether the head var of bt' is x or not. *)

type arity = 
  [ Absent 
  | Present of int
  ]
and arities = list arity
;
(* relevant : the ith var at level is outermost bound *)
value relevant (i,level) = 
  i=Index(level,1)
;
(* subeta : find son modulo eta possibly *)
value subeta l k n p display = 
  if k<=p then evaluate display (List.nth l (k-1))
  else Hnf(0,Index(1,n+k-p),[])
;
value rec access_tree (display,level) stack hnf =
  match hnf with 
    [ Hnf(n,i,args) ->
      let b = relevant(i,level) in fun
      [ []        -> (hnf,b,List.rev stack)
      | [k::path] -> 
        let p = List.length args 
        and display' = [n::display] in
        let b'= subeta args k n p display'
        and inspect = if b then Present p else Absent in
        access_tree (display',level+1) [ inspect :: stack ] b' path
      ]
    | Future _ -> error "Access_tree expects hnf"
    ]
;
(* Let t be a closed term, bt a Bohm tree.
   We say that path is accessible in t towards bt modulo eta 
   iff access t path = (bt,b,stack) for some b and stack, with: *)

value access t = fun
  [ Path p -> access_tree ([],0) [] (approximate t) p ]
;
value shape = fun
 [ Hnf(n,i,b) -> (n,i,List.length b)
 | _          -> error "Not in Head Normal Form"
 ]
;
(* similar Bohm trees in Hnf form *)
value similar (b,b') =
  let (n,i,p) = shape b 
  and (n',i',p') = shape b' in
  i=i' && p+n'=p'+n
;
(* Definition. Let t and t' be two closed terms, p be such that 
(access t p) = (b,_,_) and (access t' p) = (b',_,_). 
We say that p distinguishes t and t' iff not (similar(B,B')).

Proposition. If t and s are distinguishable by some path, then they
are separable.

The proof of this proposition is given by the Bohm-out algorithm below.

We start with a few parametric combinators *)

value tuple n = abstract (n+1) (applist (Util.range n))
    where rec applist = fun
       [ []     -> Ref 0
       | [n::l] -> App (applist l) (Ref n)
       ]
;
assert ((tuple 3 = <<[x1,x2,x3,x4](x4 x1 x2 x3)>>)
     && (tuple 2 = _Pair)
     && (tuple 0 = _I))
;
value pi k n = (* projection combinators *)
  if n>=k then abstract n (Ref (n-k)) 
  else error "Wrong projection"
;
assert ((pi 1 1 = _I) && (pi 1 2 = _True) && (pi 2 2 = _False))
;
value k_ n = pi 1 (n+1)
;
assert (k_ 1 = _K)
;
(* constant terms *)
value cst x n = nf(App (k_ n) x)
;
value ff = cst _False 
and tt = cst _True
and ii = cst _I
;
(* dupl x 3 = [x;x;x] *)
value dupl x = drec 
where rec drec = fun 
  [ 0 -> [] | n -> [ x :: drec(n-1) ] ]
;
value df = dupl _False 
and dt = dupl _True 
and di = dupl _I
;
(* First an exercise: semi-separability.
Consider the head normal form t=[x1,x2, ... , xn](xi t1 ... tp).
There exists terms a1, a2, ... , an such that (t a1 a2 ... an) ->* _I.
Proof.
The list [a1; a2; ... ; an] is computed by identity(n,i-1,p) below. *)

value identity (n,i,p) = 
  di(i) @ [ ii(p) :: di(n-i) ]
;
value semi_sep t = 
  let (n,ind,p) = shape(approximate t) in
  match ind with [ Index(0,i) -> identity(n,i-1,p) 
                 | _ -> error "semi_shape expects closed term"
                 ]
;
value project t context = 
  nf (List.fold_left (fun t s -> App t s) t context)
;
(* let _Y = <<[f]([x](f (x x)) [x](f (x x)))>> in project _Y (semi_sep _Y) 
   = _I; *)

(* Now separability *)

value sep_base1 i i' j k f g n = 
  di(i-1) @ [ f(j) :: di(i'-i-1) @ [ g(k) :: di(n-i') ] ]
;
value sep1(i,i',p,p',n,n') = (* Assumes i<>i' *)
  if n>=n' then let k=p'+n-n' in
                if i<i' then sep_base1 i i' p k tt ff n
                        else sep_base1 i' i k p ff tt n
           else let k=p+n'-n in
                if i<i' then sep_base1 i i' k p' tt ff n'
                        else sep_base1 i' i p' k ff tt n'
;

(* Separating two head normal forms [x1, x2, ..., xn](x1 t1 ... tp)
   and [x1, x2, ..., xn'](x1 s1 ... sp') with p+n'<>p'+n.
 By case analysis, using the identity:
   (true_1 true_2 ... true_k x) red true if k even>0,
                                red (true x) if k odd. *)
value sep2(p,p',n,n') = (* Assumes p+n'<>p'+n *)
 let d = n+p'-p-n' in
 let f(k,l) = if even d then [ ff(k) :: dt(l-1) @ [_False; _True ] ]
                        else [ tt(k) :: dt(l-1) @ [_True; _I] ]
 and g(k,l) = if even d then [ ff(k) :: dt(l-1) ]
                        else [ tt(k) :: dt(l-1) @ [_I] ] in
 if p>p' then if d>0 then f(p,n)
                     else g(p,n'+p-p')
              else if d>0 then f(p',n+p'-p)
                   else g(p',n')
;
(* get : the i-th element of path p *)
value get i = fun
  [ Path p ->  List.nth p (i-1) ]
;
(* skip : remove the i-th element of path p *)
value skip i = fun
  [ Path path -> Path (coll_rec 1 path)
    where rec coll_rec lev = fun
     [ []             -> error "Erroneous skip"
     | [ dir :: path] -> if lev=i then path
                         else [ dir :: coll_rec (lev+1) path ]
     ]
  ]
;
type item = 
  [ None 
  | Once of (int * int) (* i,p *)
  | Several of int   (* maxp *)
  ]
;

(* analyse occurrences of first variable as head variable along path *)
value analyse (n,n',p,p',b,b')  = anal 1
where rec anal lev = fun
  [ ([],[]) -> 
       if b then if b' then Several (max p p')
                       else Several (if n'>n then p+n'-n else p)
       else if b' then Several (if n>n' then p'+n-n' else p')
                  else None
  | ([ Present p :: st1 ],[ Present p' :: st2 ]) -> 
       match anal (lev+1) (st1,st2) with
         [ Several maxp -> Several (max p (max p' maxp))
         | Once (_,pi)  -> Several (max p (max p' pi))
         | None         -> Once (lev,max p p')
         ]
  | ( [Absent :: st1 ],[ Absent :: st2 ]) -> anal (lev+1) (st1,st2)
  | _ -> error "Non similarity along path"
  ]
;
(* separate: the Bohm-out algorithm; we assume the Bohm trees of t and s
are accessible by a minimal path leading to non-similar trees *)
value rec separate (t,s) path = 
  let (bo,b,st)    = access t path
  and (bo',b',st') = access s path in
  let (n,v,p)    = shape bo
  and (n',v',p') = shape bo' in
  if path=Path [] then (* The Bohm trees of t and s are not similar *)
     let (i,i') = match (v,v') with
        [ (Index(0,i),Index(0,i')) -> (i,i')
        | _ -> error "Non closed term" 
        ] in
       if i<>i' then sep1(i,i',p,p',n,n')
       else if p+n'<>p'+n then di(i-1) @ sep2(p,p',n,n')
            else error "Similar trees"
  else match analyse (n,n',p,p',b,b') (st,st') with
         [ None          -> let any=_I in (* Any term would do *)
              build_context any (t,s) path
         | Once(l,p)     -> (* head var appears just once on path *)
              let k = get l path in
              build_context (pi k p) (t,s) (skip l path)
         | Several(maxp) -> build_context (tuple maxp) (t,s) path
         ]
and build_context c (t,s) path = 
  [ c :: separate (App t c, App s c) path ]
;
(* Well-foundedness of separate. By induction on triple(path)=(s,c,n1)
where s=length(path), c is the number of variables at first level which have
several levels relevant, and n1 is the number of first level variables. *)

(* Remark. We use tupling operators to rename multiple occurrences of
first variable along the path, as collected by analyse. Actually this
renaming is not always necessary; when analyse considers a set of occurrences
(i,p) such that i<length(path) and same k at every level i, we could directly
apply (pi k p), and get a shorther context. 

Note that we can separate by using only pi and tuple combinators *)

value separates context (t,s) =
   project t context = _True && 
   project s context = _False;


(* Searching a path separating two trees *)
value tryfind f = findrec
  where rec findrec = fun
   [ []         -> error "Not separable"
   | [ x :: l ] -> try f x with [ Failure _ -> findrec l ]
   ]
;
value rec search_path trees (dis,dis') path = 
  match trees with 
  [ (Hnf(n,_,l), Hnf(n',_,l')) ->
    if similar trees then 
      let p = List.length l 
      and d = [ n :: dis ]
      and p' = List.length l' 
      and d' = [ n' :: dis' ] in
      let check k = 
        let hnf = subeta l k n p d 
        and hnf' = subeta l' k n' p' d' in
        search_path (hnf,hnf') (d,d')  [k :: path ] in
      tryfind check (Util.range (max p p'))
    else List.rev path
  | _ -> error "search_path expects hnfs"
  ]
;
(* Note : similarity does not depend on the display. Further we do not need
   to keep track of eta-expansions. This is an essential property of
   our representation of variables with constructor Index. *)

(* search_path searchs in a depth-first, left-to-right manner *)

(* separ_path : finding a path separating two closed terms *)
value separ_path (t,s) =
    Path (search_path (approximate t, approximate s) ([],[]) [])
;
(* Loops:  
separ_path(_Y,_Fix);
separ_path(_I,_J);
*)

value bohm (t,s) = separate (t,s) (separ_path (t,s))
;
(*

 bohm(_Delta,<<[x,y](x y y)>>); (* Z *)
 bohm(_I,_Pair);
 bohm(_Zero,_One);
 bohm(_I,_Y);
 bohm(_S,_Fix);
 bohm(<<[u](u ^I (u ^I ^I))>>,<<[u](u ^I (u u ^I))>>);
 bohm(<<[u,v](v [w](u w))>>,<<[u,v](v [w](u u))>>);
 bohm(<<[u](u (u u u) u)>>,<<[u](u (u u [u,v,w](w u v)) u)>>);

*)

value bohm_check (t,s) = 
  let context = separate (t,s) (separ_path (t,s)) in
  separates context (t,s)
;
assert (bohm_check (<<[x](x x x)>>,<<[x](x x)>>));
assert (bohm_check (<<[x](x x)>>,<<[x](x x x)>>));
assert (bohm_check (<<[x,y](x x x x)>>,<<[x](x x)>>));
assert (bohm_check (<<[x](x x)>>,<<[x,y](x x x x)>>));


