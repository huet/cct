(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Format;
open Util;
open Term;

(* Concrete syntax for lambda-expressions *)
type expression =
   [ Var of string
   | Lambda of string and expression
   | Apply of expression and expression
   ]
;
(***********)
(* Parsing *)
(***********)

(* environment *)
type environment = list string
;
(* Computing an index in an environment *)
value index_of (id:string) = search 0
where rec search n = fun
  [ [ name :: names ] -> if name=id then n 
                         else search (n+1) names
  | []                -> error "Unbound identifier"
  ]
;
(* parsing terms in an environment *)
value rec parse_env env = abstract
where rec abstract = fun
 [ Var name          -> try Ref (index_of name env) 
                        with [ (Failure _) -> error "Expression not closed" ]
 | Lambda id conc    -> Abs (parse_env [ id :: env ] conc)
 | Apply conc1 conc2 -> App (abstract conc1) (abstract conc2)
 ]
;
(* parse : (expression -> term)   parses closed terms *)
value parse = parse_env []
;

(*************)
(* Unparsing *)
(*************)

(* Unparsing convention: bound variables are printed x0, x1, ... 
   Free variables are printed u0, u1, ... *)
(* lexical conventions *)
value bound_var n = "x" ^ string_of_int n
and   free_var  n = "u" ^ string_of_int n
;
(* expression *)
value expression free_vars = expr free_vars (List.length free_vars)
where rec expr env depth = fun
 [ Ref n   -> Var (if n>=depth then free_var(n-depth)
                               else List.nth env n)
 | App u v -> Apply (expr env depth u) (expr env depth v)
 | Abs t   -> let x = bound_var depth in
              Lambda x (expr [x::env] (depth+1) t)
 ]
;
(* match <<[u][z,y][x,x'](x x' y z u)>> with
    [ Abs (Abs (Abs l)) -> expression ["y";"z"] l
    | _ -> error "Parser anomaly"
    ];
   = [x2][x3](x2 x3 y z u0)  *)

(* unparse : term -> expression *)
value unparse = expression []
;
(***********)
(* Printer *)
(***********)

(* Prints bound variables as x0,x1,... and free variables as u0,u1,... *)
value rec print_term t = print_rec [] 0 t
where rec print_rec env depth = printer True
where rec printer flag = fun
 [ Ref n   -> printf "%s" (if n>=depth then free_var (n-depth)
                              else List.nth env n)
 | App u v -> 
   let pr_app u v = do { printer False u; printf " "; printer True v } in
   if flag then do { printf "@[<1>("; pr_app u v; printf ")@]" }
           else pr_app u v
 | abs     -> let (names,body) = peel depth [] abs in
              do { print_binder (List.rev names)
                 ; print_rec (names @ env) (depth+List.length names) body
                 }
 ]
and peel depth names = fun
 [ Abs t -> let name = bound_var depth in
            peel (depth+1) [ name :: names ] t
 | other -> (names,other)
 ]
and print_binder = fun
 [ [] -> ()
 | [x::rest] -> do
   { printf "@[<1>[%s" x
   ; List.iter (fun x -> printf ",%s" x) rest
   ; printf "]@]"
   }
 ]
;
(* Ex: 
print_term <<([x,y](x (y [x]x))  [u](u u)  [v,w]v)>>;
([x0,x1](x0 (x1 [x2]x2)) [x0](x0 x0) [x0,x1]x0)

match <<[z][x](z ([y](y z)))>> 
  with [ Abs l -> print_term l
       | _     -> error "Parser anomaly"
       ];
[x0](u0 [x1](x1 u0))
*)

(* #install_printer print_term; *)
(* Now Ocaml uses print_term as printer for terms *)

