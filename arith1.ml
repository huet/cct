(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

#load "./util.cmo";
#load "./term.cmo";
#load "./expression.cmo";
#load "./lambda.cmo";

open Util;
open Term;
open Expression;
open Lambda;
open Examples;
open Arith;


(*******************************************************************)
(* Recursive naturals                                              *)
(*******************************************************************)

(* nat = !A. (nat->A)->A->A *)

(* Natural numbers a la Scott *)
value _Zero = <<[s,z]z>> (* Same as for Church *)
and _Add1 = <<[n][s,z](s n)>>
;
value scott n = iter add1 n _Zero
      where add1 _C = nf<<(!Add1 !C)>>
;
(* Numbers *)
value _Nat1 = scott 1
and _Nat2 = scott 2
;
value _Nat3 = scott 3
and _Nat4 = scott 4
and _Nat5 = scott 5
;

value _Sub1 = <<[n](n !I !Zero)>>
;
(* Semantics of a normal natural in a given interpretation *)
value eval_nat_rec iter init = eval_rec
  where rec eval_rec = fun
  [ Abs (Abs t) (* [s,z]t *) -> match t with
        [ (* z *) Ref 0 -> init
        | (* succ s *) App (Ref 1) s -> iter (eval_rec s)
        | _ -> error "Not a normal natural"
        ]
  | _ -> error "Not a normal natural"
  ]
;
(* normal_nat_rec : term -> int *)
value normal_nat_rec t = eval_nat_rec succ 0 (nf t)
;
(* Test to 0 *)
value _Eq0 = <<[n](n (!K !False) !True)>>
;
(* Recursion on natural numbers
   Recnat : nat -> (nat->A->A) -> A -> A 
   Recnat Zero s z = z
   Recnat (Add1 n) s z = (s n (Recnat n s z)) *)
value _Recnat = <<[n,s,z] let iota=[x]z in
                          let rho=[m,r](s m (m r iota r)) in
                          (n rho iota rho)>>
;
(* (Plus (Add1 n) m) = (Add1 (Plus n m))
   (Plus Zero     m) = m                *)
value _Plus = <<[n,m](!Recnat n [n,p](!Add1 p) m)>>
;
(* normal_nat_rec<<(!Plus !Nat2 !Nat2)>> = 4; *)

(* (Times (Add1 n) m) = (Plus m (Times n m))
   (Times Zero     m) = Zero                *)
value _Times = <<[n,m](!Recnat n [n,p](!Plus m p) !Zero)>>
;
(* normal_nat_rec<<(!Times !Nat5 !Nat3)>> = 15; *)

(* (Power n (Add1 m)) = (Times n (Power n m))
   (Power n Zero)     = Nat1               *)
value _Power = <<[n,m](!Recnat m [m,p](!Times n p) !Nat1)>>
;
(* normal_nat_rec<<(!Power !Nat3 !Nat3)>> = 27; *)

(* (Fact (Add1 n)) = (Times (Add1 n) (Fact n))
   (Fact zero)     = Nat1                     *)
value _Fact'' = <<[n](!Recnat n [n,p](!Times (!Add1 n) p) !Nat1)>>
;
(* normal_nat_rec<<(!Fact'' !Nat3)>> = 6; *)

(* Comparison with Church's naturals: see
M. Parigot. On the representation of data in lambda-calculus *)

