(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

#load "./util.cmo";
#load "./term.cmo";
#load "./expression.cmo";
#load "./lambda.cmo";

open Term;
open Expression;
open Lambda;

(* Standard combinators *)
value _I = <<[x]x>>                (* Identity *)
and   _K = <<[x,y]x>>              (* Constant generator (Kestrel) *)
and   _S = <<[x,y,z](x z (y z))>>  (* Shonfinkel's composition (Stirling) *)
and   _A = <<[f,x](f x)>>          (* Application *)
and   _B = <<[f,g,x](f (g x))>>    (* Composition *)
and   _C = <<[f,x,y](f y x)>>      (* Transposition *)
;

(* Paradoxical combinators *)

value _Delta = <<[x](x x)>>;
value _Omega = <<(!Delta !Delta)>>;
value _Y = <<[f]([x](f (x x)) [x](f (x x)))>>;

(* Omega conv (Y I) *)

(* J not separable from i *)
value _J = <<(!Y [u,x,y](x (u y)))>>;


(********************)
(* Counter examples *)
(********************)

(* Omega has no whnf, it is totally undefined *)

(* absOmega has a whnf (itself), but no hnf (i.e. hnf(absOmega) loops) *)
value absOmega = <<[x]!Omega>>;
(* absOmega has a wnf, but no nf; it has a vwnf, but no vnf *)

(* wide has a whnf [x]t, t has a whnf, etc, but wide has no hnf *)
value wide = <<(!Y [e][x]e)>>;

(* delay has a hnf (itself), but no nf *)
value delay = <<[x](x !Omega)>>;

(* Y has a hnf [x](x (y x)), (Y x)  has a hnf (x (y x)), etc, but Y has no nf.
Its Bohm tree is [x](x (x ... )) *)

(* Similarly deep has for Bohm tree [x1](x1 [x2](x2 [x3](x3 ... ))) *)
value deep = <<(!Y [e][x](x e))>>;

(* app_Omega has a hnf, but no wnf *)
value app_Omega = (App (Ref 0) _Omega);

(* danger has a nf (_I), but no vnf (not even a vwnf) *)
value danger = <<(!K !I !Omega)>>;

(* caution and all its subterms have a nf, but it has no vnf *)
value caution = <<([x]([y]!I (x x)) !Delta)>>;


