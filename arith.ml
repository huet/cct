(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

#load "./util.cmo";
#load "./term.cmo";
#load "./expression.cmo";
#load "./lambda.cmo";

open Util;
open Term;
open Expression;
open Lambda;
open Examples;


(*******************************************************************)
(* Computability theory : Encoding primitive recursive functionals *)
(*******************************************************************)

(************)
(* Booleans *)
(************)

(* Bool *)
value _True =  <<[x,y]x>>   (* same as _K *)
and _False = <<[x,y]y>>     (* conv <<(!K !I)>> *)
and _Cond = <<[p,x,y](p x y)>>
;
(* Pairs *)
value _Pair = <<[x,y,p](p x y)>> (* conv <<[x,y,p](!Cond p x y)>> *)
and _Fst = <<[p](p !True)>> 
and _Snd = <<[p](p !False)>>
;
(* Nat : Church's natural numbers *)
value _Zero = <<[s,z]z>> (* same as _False *)
and _Succ = <<[n][s,z](s (n s z))>>
;
(* Church *)
value church n = iter s n _Zero
  where s _C = nf<<(!Succ !C)>>
;
(* Numbers *)
value _One = church 1
and _Two =   church 2
and _Three = church 3
and _Four =  church 4
and _Five =  church 5
;
(* Church's naturals are functional iterators i.e. for loops *)
value eval_nat iter init = fun
  [ Abs (Abs t) (* [s,z]t *) -> eval_rec t
       where rec eval_rec = fun
        [ (* z *) Ref 0 -> init
        | (* (s u) *) App (Ref 1) u -> iter (eval_rec u)
        | _ -> error "Not a normal church natural"
        ]
  | _ -> error "Not a normal church natural"
  ]
;
(* Some examples of use of eval_nat.

value roman = eval_nat (fun s -> "|" ! s) "";
type quadtree = [ Q | Q4 of (quadtree * quadtree * quadtree * quadtree) ];
value quad n = eval_nat (fun t -> Q4(t,t,t,t)) Q (church n);
*)

(* Standard interpretation *)
value compute_nat = eval_nat succ 0
;
(* normal_nat : term -> int *)
value normal_nat n = compute_nat (nf n)
;
(* (eval_nat f x (church n)) is equivalent to (iter f n x) *)

(* Null : Test to 0 *)
value _Null = <<[n](n (!K !False) !True)>>
;
(* Addition is composition *)
value _Add = <<[m,n][s,z](m s (n s z))>>
;
(* normal_nat<<(!Add !Two !Two)>> = 4; *)

(* Remark. There are many possible encodings of natural numbers in term.
For a given encoding, there are many algorithms computing extensionally the
same function. For instance, here is another successor algorithm, which
normalizes its result in applicative order: *)

value _Succv = <<[n][s,z](n s (s z))>>
;
(* Another addition algorithm : <<[m,n](m !Succ n)>> .
Still another one :             <<[m,n](m !Succ [s,z](n s z))>>
The applicative version: *)
value _Addv = <<[m,n](m !Succv [s,z](n s z))>>
;
(* Other versions obtainable by interchanging m and n, of course *)

(* Multiplication is composition *)
value _Mult = _B
;
(* normal_nat<<(!Mult !Five !Five)>> = 25; *)

(* Another multiplication algorithm : <<[m,n](m (!Add n) !Zero)>> *)

(* Exponentiation is the tranposed of application *)
value _Exp = <<[m,n](n m)>>
;
(* normal_nat<<(!Exp !Three !Five)>> = 243; *)

(* Another exponentiation algorithm : <<[m,n](n (!Mult m) !One)>> *)

(* Primitive recursion *)
(* We iterate the computation on the pair (n,fact n) *)

value _Fact = <<let loop = [p] let n = (!Succ (!Fst p)) in
                               (!Pair n (!Mult n (!Snd p))) in
                [n](!Snd (n loop (!Pair !Zero !One)))>>
;
(* normal_nat<<(!Fact !Three)>> = 6; *)

(* Fibonacci *)
value _Fib = <<let loop = [p] let fib1 = (!Fst p)
                              in (!Pair (!Add fib1 (!Snd p)) fib1)
               in [n](!Snd (n loop (!Pair !One !One)))>>
;
(* normal_nat<<(!Fib !Five)>> = 8; *)

(* Actually, the same idea is used to compute the predecessor *)

(* Pred :  Kleene's predecessor *)
value _Pred = <<let loop = [p] let pred = (!Fst p)
                               in (!Pair (!Succ pred) pred) 
                in [n](!Snd (n loop (!Pair !Zero !Zero)))>>
;
(* >= *)
value _Geq = <<[m,n](m !Pred n (!K !False) !True)>>
;
(* Going to the full type hierarchy : general arithmetical functions *)

(* Actually, iteration is stronger than primitive recursion if we
   iterate functional values *)

(* Functional iteration *)
value _Iter = <<[f][n](n f (f !One))>>
;
(* Ackermann's function *)
value ackerman n = ack2(n,n)
where rec ack2 = fun
  [ (0,n) -> n+1
  | (m,0) -> ack2(m-1,1)
  | (m,n) -> ack2(m-1,ack2(m,n-1))
  ];
 
(* ack *)
value _Ack = <<[n](n !Iter !Succ n)>>
;
(* normal_nat<<(!Ack !Two)>> = 7; *)


(*********************)
(* General recursion *)
(*********************)

(* We now enter the realm of partial recursive functions and functionals *)

(* Curry's fixpoint combinator *)

value _Y = <<[f]([x](f (x x)) [x](f (x x)))>>
;
(* Note that (!Y f) conv (f (!Y f)) for every term f *)

(* Turing's fixpoint combinator *)

value _Fix = <<([x,f](f (x x f)) [x,f](f (x x f)))>>
;
(* Note that now (!Fix f) reduces to (f (!Fix f)) in normal order *)

(* Recursive factorial *)
value _Fact_rec = 
    <<(!Fix [fact][n](!Cond (!Null n) !One (!Mult n (fact (!Pred n)))))>>
;
(* Fact_rec has a wnf, but no vwnf; it has a hnf, but no nf;
   the ML definition of factorial as
   value rec fact n = if n=0 then 1 else n*(fact(n-1))
   is closer to its eta-expansion <<[n](!Fact_rec n)>>; *)


(* Theorem 1. Turing completeness of lambda calculus. *)
(* Every partial recursive function f of k arguments may be represented as
a term tf which is extensionally equal to f, in the sense that for
every naturals n1, ..., nk, f(n1,...,nk) is defined and equal to n
if and only if (tf church(n1) ... church(nk)) has a normal form,
in which case it is equal to church(n). *)

(* Theorem 2. *)
(* Furthermore, if f is a total function, we may choose tf in normal form.
Note that this is not obvious from above, since for instance fact_rec
has no normal form. *)


(* These theorems show that lambda-calculus is Turing-complete. The negative
consequence of this positive fact is that many properties of lambda-calculus
computations become undecidable. See recursion module. *)


(*************************)
(* Other data structures *)
(*************************)

(* We follow the intuition from system F's representation of polymorphism :

   NAT = !A.(A->A)->A->A
   LIST B = !A.(B->A->A)->A->A 
*)

(* Lists *)
value _Nil = <<[c,n]n>>     (* same as _Zero *)
and _Cons = <<[x,l][c,n](c x (l c n))>>
;
(* list : int list -> term *)
value rec list = fun
  [ [x::l] -> let _Cx = church x and _Ll = list l in
              <<[c,n](c !Cx (!Ll c n))>>
  | []   -> _Nil
  ]
;
(* Append *)
value _Append = <<[l,l'][c,n](l c (l' c n))>>
;
(* Another possible definition of append *)
value _Append' = <<[l](l !Cons)>>
;
(* Every list acts like ML's List.fold_right functional *)
value _Fold = <<[iter,l,init](l iter init)>>
;
(* Map *)
value _Map = <<[f,l](l [x](!Cons (f x)) !Nil)>>
;
(* Length *)
value _Length = <<[l](l [x]!Succ !Zero)>>
;
(* let _L = list[1;2;3] in normal_nat<<(!Length !L)>>=3; *)

(* Pi *)
value _Pi = <<[l](l !Mult !One)>>
;
value eval_list_of_nats = fun
  [ Abs (Abs t) (* [c,n]t *) -> lrec t
      where rec lrec = fun
        [ (* n *)       Ref 0                 -> []
        | (* (c x l) *) App (App (Ref 1) x) l -> [(compute_nat x)::(lrec l)]
        | _ -> error "Not a normal List"
        ]
  | _ -> error "Not a normal List"
  ]
;
value normal_list l = eval_list_of_nats (nf l)
;
(* We recall the ML reverse function:
value rev l = rev2 [] l 
where rec rev2 acc = fun
     [ [x::l] -> rev2 [x::acc] l
     | []   -> acc
     ];
*)

value _Rev = <<[l](!Rev2 l !Nil)>>
where _Rev2 = <<[l](l [x,f][a](f (!Cons x a)) !I)>>
;
(* Range *)
value _Range = <<[n](!Rev (!Fst
              (n [p] let l = (!Fst p) in 
                     let m = (!Snd p) in
                     (!Pair (!Cons m l) (!Succ m)) (!Pair !Nil !One))))>>
;
(* normal_list<<(!Range !Four)>> = [1; 2; 3; 4]; *)
 
(* normal_list<<(!Map !Fact (!Range !Four))>> = [1; 2; 6; 24]; *)

(* Fact' :  An unusual definition of factorial *)
value _Fact' = <<(!B !Pi !Range)>>
;
(* normal_nat<<(!Fact' !Three)>> = 6; *)

(* partition p l = (l1,l2) with l1 (resp l2) the elements of l verifying p
   (resp (not p)) *)

value _Partition = 
  <<[p,l] let fork = 
            [x,pair] let l1 = (!Fst pair) in
                     let l2 = (!Snd pair) in 
                     (p x (!Pair (!Cons x l1) l2) (!Pair l1 (!Cons x l2))) in
          (l fork (!Pair !Nil !Nil))>>
;
(* normal_list<<(!Fst (!Partition (!Geq !Two) (!Range !Four)))>> = [1; 2]; *)

(* Quicksort *)
value _Quicksort = 
  <<(!Fix [q]let sort = [a,l]
               let p = (!Partition (!Geq a) l) in
               (!Append (q (!Fst p)) (!Cons a (q (!Snd p)))) in
             [l](l sort !Nil))>>
;
(* let _L=list[3;2;5;1] in normal_list<<(!Quicksort !L)>>
 = [1; 2; 3; 5]; -- (27s) *)

(* Note that quicksort has an exponential behaviour because of normal
   evaluation. Since all our computations terminate on data representations,
   we may use the applicative evaluation strategy. *)

(* applicative_list *)
value applicative_list t = eval_list_of_nats (vnf t)
;
(* let _L=list[3;2;5;1] in applicative_list<<(!Quicksort !L)>>
 = [1; 2; 3; 5]; -- (2s) *)

(* This one is much quicker; it is still exponential, though, because
the representation of data types is not adequate for simulating pattern
matching; for instance, pred is linear instead of being constant,
and (Geq m n) takes time m*n!2. There exist better representations of
data objects such as natural numbers, but they blow up to exponential size
if a proper sharing is not maintained. The de Bruijn representational 
scheme of terms is not directly suited to shared computations.

The ML computation strategy is close to the applicative strategy. However,
it is more lazy, in that it only computes the whnf of values of functional
types. Since e.g. integers are not represented as abstractions, there is no
direct analogue of ML's computation scheme in terms of pure lambda calculus. *)


(* Other representations *)

(* There are many possible representations for naturals. Here is one proposed
by Henk Barendregt. *)

(* Nat' *)
value _Zero' = _I
and _Succ' = <<[n](!Pair !False n)>>
;
(* Barendregt *)
value rec barendregt n = if n=0 then _I else pair _False (barendregt (n-1))
  where pair _L1 _L2 = nf<<(!Pair !L1 !L2)>>
;
value _Null' = <<[n](n !True)>>
;
(* Remark that predecessor is now easy to compute: *)

value _Pred' = <<[n](n !True !Zero' (n !False))>>
;
(* let _One'= <<(!Succ' !Zero')>> in nf<<(!Pred' !One')>> = _Zero'; *)

(* Exercise. Program addition and multiplication with Barendregt's naturals. *)

(* We finish with a proposal of Michel Parigot *)

(* Parigot's naturals *)
value _Zero'' = <<[s,z]z>>
and _Succ'' = <<[n][x,s](s n x)>>
;
(* Exercise. Program addition and multiplication with Parigot's naturals. *)

