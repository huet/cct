(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Util;

(* term : abstract syntax *)
type term =
   [ Ref of int             (* variables as reference depth *)
   | Abs of term            (* abstraction [x]t             *)
   | App of term and term   (* application (t u)            *)
   ]
;
value abs t = Abs t
and app f x = App f x
;
value abstract n = iter abs n
;
(***********)
(* Closure *)
(***********)

(* term valid in a context of n free variables *)
value rec valid_at_depth n = fun
   [ Ref m   -> m < n
   | Abs t   -> valid_at_depth (n+1) t
   | App t u -> valid_at_depth n t && valid_at_depth n u
   ]
;
value closed_term = valid_at_depth 0
;
(* (valid_at_depth n t) <=> (closed_term (abstract n t)) *)

value min_context_depth = min_rec 0 
  where rec min_rec n = fun
    [ Ref m   -> m-n+1
    | Abs t   -> min_rec (n+1) t
    | App t u -> max (min_rec n t) (min_rec n u)
    ]
;
value closure t = abstract (min_context_depth t) t
;
(****************)
(* Substitution *)
(****************)

(* Recomputes references of global variables across n levels of bindings. *)
(*  If (valid_at_depth n t), then (valid_at_depth (n+k) (lift k t)). *)

value lift n = lift_rec 0 
where rec lift_rec k = lift_k
where rec lift_k = fun
  [ Ref i   -> if i<k then Ref(i)      (* bound variables are invariant *)
                      else Ref(n+i)    (* free variables are relocated by n *)
  | Abs t   -> Abs (lift_rec (k+1) t)
  | App t u -> App (lift_k t) (lift_k u)
  ]
;
assert (lift 1 (Abs (App (Ref 0) (Ref 1))) = (Abs (App (Ref 0) (Ref 2))))
;
(* The substitution function. Let n,t,u be such that (valid_at_depth (n+1) t)
   and (valid_at_depth n u). We substitute u for Ref(1) in t by (subst u t).
   The typical case is ([x]t u), which beta-reduces to (subst u t). *)

value subst w = subst_w 0
where rec subst_w n = fun
 [ Ref k   -> if k=n then lift n w      (* substituted variable *)
              else if k<n then Ref k    (* bound variables *)
                   else Ref (k-1)       (* free variables *)
 | Abs t   -> Abs (subst_w (n+1) t)
 | App t u -> App (subst_w n t) (subst_w n u)
 ]
;
(* Example: 
  let f = <<[x][y](y [z](x z))>>
  and x = <<[z]z>> in
  match f with [ Abs body -> subst x body | _ -> fail() ];
i.e. subst (Abs (Ref 0)) (Abs (App (Ref 0) (Abs (App (Ref 2) (Ref 0)))))
= Abs (App (Ref 0) (Abs (App (Abs (Ref 0)) (Ref 0))))
= << [y](y [z]([u]u z)) >> *)


(**************************)
(* Computation strategies *)
(**************************)

(* 1. Normal order or call by need or lazy evaluation *)

(* 1.1. Head normal form *)

(* a. Strong reduction *)

(* Reduce a term to head normal form *)

value rec hnf = fun
  [ Ref n   -> Ref n
  | Abs t   -> Abs (hnf t)
  | App t u -> match hnf t with
      [ Abs w -> hnf (subst u w)
      | h     -> App h u
      ]
  ]
;
(* A term t is said to be defined or solvable if hnf(t) terminates *)

(* b. Weak reduction *)

(* Weak head normal form *)

value rec whnf = fun
  [ App t u -> match whnf t with
     [ Abs w -> whnf (subst u w)
     | h     -> App h u
     ]
  | x -> x
  ]
;
(* 1.2. Normal form *)

(* a. Strong reduction *)

(* Normal-order normal form *)

value rec nf = fun
  [ Ref n   -> Ref n
  | Abs t   -> Abs (nf t)
  | App t u -> match hnf t with
     [ Abs w  -> nf (subst u w)
     | h      -> App (nf h) (nf u)
     ]
  ]
;
(* b. Weak reduction *)

(* Weak normal form - not usual *)

value rec wnf = fun
  [ App t u -> match whnf t with
     [ Abs w  -> wnf (subst u w)
     | h      -> App (wnf h) (wnf u)
     ]
  | x       -> x
  ]
;

(* 2. Applicative order or call by value or eager evaluation *)

(* a. Strong reduction *)

(* Applicative-order normal form *)

value rec vnf = fun
  [ Ref n   -> Ref n
  | Abs t   -> Abs (vnf t)
  | App t u -> let v = vnf u in
               match whnf t with (* whnf ! *)
     [ Abs w -> vnf (subst v w)
     | h     -> App (vnf h) v
     ]
  ]
;
(* b. Weak reduction *)

(* Applicative-order weak normal form *)

value rec vwnf = fun
  [ App f x -> let vx = vwnf x in
               match vwnf f with
     [ Abs t -> vwnf (subst vx t)
     | t     -> App t vx
     ]
  | t       -> t
  ]
;
(* Note: ML basically uses the vwnf evaluation strategy *)


(**************************************)
(* Lambda conversion by normalisation *)
(**************************************)

value rec conv t s = match (whnf t , whnf s) with
  [ (Ref m , Ref n)         -> m=n
  | (Abs t , Abs s)         -> conv t s
  | (App t1 t2 , App s1 s2) -> (conv t1 s1) && (conv t2 s2)
  | _                       -> False
  ]
;
(* Caution. (conv t u) may not terminate if t and u do not have normal forms,
although t and u may be proved to be inter-convertible by beta reduction.
Consider for instance Omega and (K Omega I).
A more general conversion relation would need a non-deterministic exploration
of beta-reduction and expansion trees from say t, testing for equality with u.
No total such relation is recursively definable though, since conversion is
undecidable, as we shall see. *)


