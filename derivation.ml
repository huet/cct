(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Util;
open Term;
open Expression;
open Lambda;
open Reduction;
open Redexes;
open Cube;

(************************)
(* Parallel derivations *)
(************************)

(* We use lists of redex sets to represent parallel derivation sequences. *)

type sequence = list redexes
;

(*************************************)
(* (Parallel) derivations from terms *)
(*************************************)

type derivation = [ Der of (term * sequence) ];

(* start_term and end_term *)
value start_term = fun
  [ (Der(t,_)) ->  t ]
and end_term = fun
  [ (Der(t,s)) -> List.fold_left par_reduce t s ]
;
(* sequence of redexes of a derivation *)
value sequence = fun [ (Der(_,s)) -> s ]
;
value der_length d = List.length (sequence d)
;
(* A derivation d is said to be consistent iff end_term succeeds; 
   We say that the type of d is the pair (start_term t, end_term d). *) 

(* identity : empty derivation issued from term M *)
value identity t = Der(t,[])
;
(* derivations composition *)
value compose d1 d2 =
 match d1 with [ Der(t1,s1) -> 
 match d2 with [ Der(t2,s2) ->
    if t2 = (end_term d1) then Der(t1,s1@s2)
    else error "Derivations not composable" ]]
;
(* is_empty : derivation composed of empty steps *)
value is_empty = fun
  [ Der(_,s) ->  List.for_all is_void s ]
;
(* empty derivation of length n issued from term t *)
value empty_der t n = Der(t,iter (cons (void t)) n [])
;

(********************)
(* Residual algebra *)
(********************)

(* res : residual of a redex set by a sequence *)
value rec res vv = fun
  [ []        -> vv
  | [uu :: s] -> res (derive vv uu) s
  ];

(* residual of a redex set by a derivation *)
value res_der vv = fun
  [ Der(t,s) -> 
    if is_marking(vv,t) then res vv s 
    else error "Incompatible redex set"]
;
(* res_by : residual by a redex set of a sequence *)
value rec res_by vv = fun
  [ []        -> []
  | [uu :: s] -> [(derive uu vv) :: (res_by (derive vv uu) s)]
  ]
;
(* Note that List.length(res_by e s) = List.length(s) *)

(* der_res : residual of a derivation by a compatible redex set *)
value der_res d vv = match d with
 [ Der(t,s) -> 
  if is_marking(vv,t) then Der(par_reduce t vv,res_by vv s) 
  else error "Derivations not co-initial"]
;
(* res_seq : residual of a sequence by a sequence *)
value rec res_seq s1 = fun
  [ []       -> s1
  | [vv::s2] -> res_seq (res_by vv s1) s2
  ]
;
(* residual of a derivation by a co-initial derivation *)
value residual d1 d2 = 
  match d1 with [ Der(t1,s1) ->
  match d2 with [ Der(t2,s2) -> 
    if t1=t2 then Der(t1,res_seq s1 s2)
    else error "Derivations not co-initial"]]
;
(* Note that der_length(residual d1 d2) = der_length(d1) *)

(* Proposition. For all d,d1,d2 of proper type:
   residual d (compose d1 d2) = residual (residual d d1) d2.          (D1)
   residual (compose d1 d2) d = compose (residual d1 d) 
                                      (residual d2 (residual d d1)).  (D2)
   residual d d = empty_der (end_term d) (der_length d).              (D3)
*)

(* Derivation preordering : d2 extends d1 *)

value leq d1 d2 = is_empty (residual d1 d2)
;
(* Proposition. leq is a partial ordering, verifying:
   (leq d1 d2) => (all d) (leq (compose d d1) (compose d d2)).        (D4)
   (leq d1 d2) => (all d) (leq d1 (compose d2 d)).                    (D5)
   leq d (compose d d').                                              (D6)
*)

(* Derivations equivalence *)

value equiv d1 d2 = (leq d1 d2) && (leq d2 d1)
;
(* Equivalent derivations begin and end with the same terms. But the
   converse is not true, like the (I (y y)) example shows:
value r1 = mark "[y](#[x]x ([x]x y))" 
and r2 = mark "[y]([x]x (#[x]x y))";

r1\r2=r2\r1; (* = mark "[y](#[x]x y)" *)
Thus with t=<<[x](^I (^I x))>>,d1=Der(t,[r1]) and d2=Der(t,[r2]),
we have end_term(d1)=end_term(d2) but not (equiv d1 d2). 

However, we have:
Proposition. Let d and d' be any two derivations starting from a term t
and ending at the normal form of t. Then equiv d d'. *)


(* Proposition. For all d,d' of the right type,
   (is_empty d) => equiv d' (compose d d').                           (D7)
   (is_empty d) => eqiiv d' (compose d' d).                           (D8)
*)

(* Other characterisation of the derivation equivalence equiv.
equiv d d' iff d can be transformed into d' by diamond permutation
(i.e. commuting steps [uu; vv\uu] and [vv; uu\vv]) and pasting empty steps. 
For this reason, it is called the permutation equivalence. *)

(* der_union : union of two co-initial derivations *)
value der_union d d' = compose d (residual d' d)
;
(* The cube lemma implies that
    equiv (der_union d d') (der_union d' d).                           (D9)
For all co-initial derivations d and d', we have leq d (der_union d d')
and leq d' (der_union d d'). Furthermore, (der_union d d') is the least
such derivation. *)

(* Proposition.
leq d1 d2 iff (exists d) equiv d2 (compose d1 d).                      (D10)
Proof.
<= Use D6
=> Take d=(residual d2 d1). Use D9.
*)


(****************************)
(* The derivations category *)
(****************************)

(* The objects of the category are the terms *)

(* The maps from t to t' are equivalence classes under equiv 
   of the set of derivations starting from t and ending in t'. *)

(* The categorical diamond theorem (Levy, 1976).
The derivations category admits pushouts. *)

(* This means that the diamond lemma is a much stronger algebraic closure
than just stating the confluence of the reduction relation: the confluence 
diagram is a commuting diagram, modulo permutation equivalence. *)


(**********************)
(* Single derivations *)
(**********************)


(* Derivation composed of singleton steps *)
value is_single = fun
 [ Der(_,s) -> 
  let is_singleton = fun [[_] -> True | _ -> False] in
  List.for_all (fun x -> is_singleton (marks x)) s ]
;
(* Mapping a derivation sequence to a list of single steps redex positions *)
value singles = List.map extract
where extract(uu) = match marks (uu) with
    [ [u] -> u 
    | _   -> error "Not single"
    ]
;
(* All the above theory of parallel derivations applies to sequences of
single-step derivations. Just map the single-step reducing term t 
at position u into the singleton set of redexes [u] = singleton(t,u). *)

(* single_res : residual of a derivation by a single redex *)
value single_res d u = 
 match d with [ Der(t,s) -> 
  Der(reduce t u, res_by (singleton(t,u)) s) ]
;
(* More generally, a sequence s of redexes positions and a term t 
   determine a single derivation sder(t,s) as follows. *)

value sder(t,s) = 
  let simul (t',seq) u = (reduce t' u, [singleton(t',u)::seq]) in
  Der(t, List.rev(snd(List.fold_left simul (t,[]) s)))
;
(* Note that is_single(sder(t,s)) = True; *)
(* Example. 
let t = <<[u1]([z](z z) [y]([x]y u1))>> 
and s = [[F; A R; F]; [F]] in sder(t,s);
- : Derivation.derivation = Der([x0]([x1](x1 x1) [x1]([x2]x1 x0)),
  [[x1]([x2](x2 x2) [x2](#[x3]x2 x1)); [x1](#[x2](x2 x2) [x2]x2)])
*)

(* Conversely, any set of redexes uu may be transformed into a sequence of 
 single-steps reductions by choosing any developement in which r is
 singleton at every step. The finite developments theorem
 below states that any such transformation terminates. *)

