(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

#load "./util.cmo";
#load "./term.cmo";
#load "./expression.cmo";
#load "./lambda.cmo";

open Util;
open Term;
open Expression;
open Lambda;
open Examples;
open Arith;

(* Rice theorem in lambda calculus *)

(* First some preliminaries on Godel numberings *)

(* Cantor : encoding of pairing as a bijection N*N~N *)
value cantor (n,m) = m+(n+m)*(n+m+1)/2
;
(* proj(cantor p)=p (p:N*N) and cantor(proj(n)=n (n:N) *)
value proj p = dec (0,0) 
  where rec dec (sum,sigma) =
   (* sum=n+m, sigma=0+1+...+n, where p=cantor(n,m)=sigma+m *)
   let m=p-sigma in 
   if sum>=m then (sum-m,m) 
   else dec (sum+1,sigma+sum+1)
;
(* Godel numbering of term as an injection into positive integers *)
value rec godel = fun
  [ Ref n   -> 2*n+1
  | Abs t   -> 2*cantor(0,godel t)
  | App t u -> 2*cantor(godel t,godel u)
  ]
;
(* ungodel (godel t) = t *)
value rec ungodel g = 
  if g<=0 then error "Godel numbers are positive"
  else if odd g then Ref ((g-1)/2)
  else let (m,n) = proj(g/2) in
       if m=0 then Abs (ungodel n)
       else App (ungodel m) (ungodel n)
;
(* godel _Delta = 88 && godel _Omega = 31328 && _Omega = ungodel 31328; *)

(* Godel numbers become large very quickly - we need exact arithmetic *)
(* godel _Fix = 6941718342796165477078794502929179108365127687513804648; *)

(* Kleene brackets [|t|] compose Church with Godel *)
value kleene t = church (godel t)
;
(* kleene _Delta = church (godel _Delta); *)

(* Proposition 1. There exists a term g1 which represents the
arithmetic function: (fun (x,y) -> 2*cantor(x,y)), in the sense
that (g1 (church n) (church m)) conv (church (2*cantor(n,m))).
Proof. By Turing completeness.
Corollary. conv (g1 (kleene t) (kleene u)) (kleene (App t u))

Proposition 2. There exists a term g2 which represents the
arithmetic function: (fun x -> godel(church x)), in the sense
that (g2 (church n)) conv (kleene (church n)).
Proof. Idem.
Corollary. (g2 (kleene t)) conv (klenee (kleene t)).

Definition. Kleene-fixpoint. The term u is a Kleene-fixpoint
of term t iff (t (kleene u)) conv u.

(Second) fixpoint theorem. Every term t possesses a Kleene-fixpoint u.
Proof. Take u=(h (kleene h)), with h=[n](t (g1 n (g2 n))).
We get u=(h (kleene h))=([n](t (g1 n (g2 n))) (kleene h))
        red  (t (g1 (kleene h) (g2 (kleene h))))
        conv (t (g1 (kleene h) (kleene (kleene h)))) by g2
        conv (t (kleene u)) by g1.

Definition. Recursive set. A set A of natural numbers is said to be
recursive iff there exists a recursive total predicate In_A
such that In_A(n) iff n is in A.
By extension, we say that a set A of terms is recursive iff the set
of its Godel numberings is recursive.

Theorem of Rice-Scott. The only recursive sets of terms closed under
beta-conversion are the empty set and the set of all terms.

Proof. Assume A is closed under conversion, with t in A, and s not in A.
Assume there is a recursive total predicate P such that (P (godel t)) is
true, and (P (godel s)) is false. By Turing completeness, there exists a
term _P which represents P, i.e. such that
(_P (kleene t)) conv _True  if t is in A, and
(_P (kleene t)) conv _False otherwise.
Now consider the term f=[x](_P x s t).
We get  (f (kleene t)) red (_P (kleene t) s t) conv s  if t is in A,
and similarly (f (kleene t)) conv t otherwise. The Kleene-fixpoint of f
contradicts the fact that A is closed under conversion.

Corollaries. It is undecidable if a term has a normal form. 
             It is undecidable if two terms are convertible.

*)
   
