(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Term;
open Expression;
open Camlp4.PreCast;
open Syntax;

(* The following grammar allows ML expressions and patterns for
   pure lambda expressions (concrete syntax)
   and closed pure lambda terms (abstract syntax with de Bruijn indexes) *)

module Gram = MakeGram Lexer;

value term_exp_eoi = Gram.Entry.mk "term"
and expr_exp_eoi = Gram.Entry.mk "lambda"
;
EXTEND Gram
  GLOBAL: term_exp_eoi expr_exp_eoi;
  term_exp_eoi:
    [ [ e = expr_exp_eoi -> <:expr< Expression.parse $e$ >> ] ];
  expr_exp_eoi:
    [ [ e = expr_exp; `EOI -> e ] ];
  expr_exp:
    [ [ "["; b = binder; "]"; e = expr_exp 
        -> let func v e = <:expr< Lambda $str:v$ $e$ >> in
           List.fold_right func b <:expr<$e$>>
      | "("; appl = lexpr_exp; ")" -> appl 
      | x = LIDENT -> <:expr< Var $str:x$ >> 
      | "let"; x = LIDENT; "="; e1 = expr_exp; "in"; e2 = expr_exp
        -> <:expr< Apply (Lambda $str:x$ $e2$) $e1$ >>
      | "!"; x = exp_antiquot -> <:expr< Expression.unparse $x$ >>
   ] ];
  lexpr_exp:
    [ [ e = expr_exp -> e
      | l = lexpr_exp; e = expr_exp -> <:expr< Apply $l$ $e$ >>
    ] ];
  binder:
    [ [ x = LIDENT -> [ x ]
      | x = LIDENT; ","; b = binder -> [ x :: b ]
    ] ];
  exp_antiquot:
    [ [ x = UIDENT -> 
          <:expr< $lid: "_" ^ x $ >>
    ] ]; 
END
;
value expr_exp = Gram.parse_string expr_exp_eoi
and   term_exp = Gram.parse_string term_exp_eoi
;
value expand_expr loc _ s = expr_exp loc s
and   expand_term loc _ s = term_exp loc s
and   expand_str_item_expr loc _ s = <:str_item@loc< $exp:expr_exp loc s$ >>
and   expand_str_item_term loc _ s = <:str_item@loc< $exp:term_exp loc s$ >>
;
Quotation.add "expr" Quotation.DynAst.expr_tag expand_expr; 
Quotation.add "term" Quotation.DynAst.expr_tag expand_term; 
Quotation.add "expr" Quotation.DynAst.str_item_tag expand_str_item_expr;
Quotation.add "term" Quotation.DynAst.str_item_tag expand_str_item_term;
Quotation.default.val := "term";

(* Examples 
<:expr<[x]x>>;
- : Expression.expression = Lambda "x" (Var "x")
<:term<[x]x>>;
- : Term.term = Abs (Ref 0)
<<[x](x x)>>; (* default quotation is term *)
- : Term.term = Abs (App (Ref 0) (Ref 0))

print_term <:term<([x,y](x (y [x]x))  [u](u u)  [v,w]v)>>;
([x0,x1](x0 (x1 [x2]x2)) [x0](x0 x0) [x0,x1]x0)

(* alpha conversion is implicit *)
assert ( <<([x,y](x (y [x]x))  [u](u u)  [v,w]v)>>
       = <<([x0,x1](x0 (x1 [x2]x2)) [x0](x0 x0) [x0,x1]x0)>>);
*)
