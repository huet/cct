# Makefile of CCT Library GH 30-07-2011

SERVICE=.ocamlinit .depend

SYNTAX=util.cmo term.cmo expression.cmo lambda.cmo 

SOURCES=util.ml term.ml expression.ml lambda.ml examples.ml arith.ml arith1.ml recursion.ml reduction.ml redexes.ml cube.ml derivation.ml develop.ml standard.ml eta.ml bohm.ml open.ml

INFOS=README ENTETE LICENSE 

DOC=CCT.tex biblio.bib 

COMPILE=ocamlopt -pp "camlp4rf" -I +camlp4 -c

COMPILEI=ocamlc -pp "camlp4rf" -I +camlp4 -c

LINK=ocamlopt -I $(ZEN) -I +camlp4 Camlp4.cmxa

all: util.cmo lambda.cmo expression.cmo term.cmo examples.cmo arith.cmo arith1.cmo recursion.cmo reduction.cmo redexes.cmo cube.cmo derivation.cmo develop.cmo standard.cmo eta.cmo bohm.cmo

# Toplevel linking the library. 
top: $(SOURCES:.ml=.cmo)
	ocamlmktop -pp "camlp4r pa_extend.cmo" -I +camlp4 dynlink.cma camlp4r.cma -o cct.exe $+
# CAUTION: remove .ocamlinit before calling cct.exe or use -init like below

toplevel: top
	./cct.exe -init open.ml 
# example of use of toplevel: bohm (_Y,_Ack); 

Makefile: 
	echo "Makefile is hand-made"

ENTETE: 
	echo "ENTETE is hand-made"

LICENSE: 
	echo "LICENSE is hand-made and should not be tampered with"

# #use "test.ml"; in order to test/debug the course notes
# .ocamlinit ought to contain lines: #load "dynlink.cma";; #load "camlp4r.cma";; 
test: util.cmo term.cmo expression.cmo lambda.cmo examples.cmo arith.cmo arith1.cmo recursion.cmo reduction.cmo redexes.cmo cube.cmo derivation.cmo develop.cmo standard.cmo eta.cmo bohm.cmo
	(cat test.ml; cat -) | ocaml 

doc: CCT.tex
	pdflatex CCT
	bibtex CCT
	pdflatex CCT

tar: 
	mkdir CCT
	cp $(SERVICE) $(SOURCES) test.ml $(INFOS) index.html $(DOC) CCT.pdf Makefile CCT/.
	tar czvf cct.tar.gz CCT
	rm -rf CCT

distr: 
	mkdir CCT
	cp $(SERVICE) $(SOURCES) test.ml $(INFOS) CCT.pdf Makefile CCT/.
	tar czvf cct.tar.gz CCT
	rm -rf CCT

depend: $(SOURCES) $(SYNTAX)
	> .depend.new
	for i in $(SOURCES); do \
	ocamldep -pp camlp4rf -I . $$i >> .depend.new ; \
	done
	mv .depend.new .depend

clean:
	rm -f *.cmo *.cmi *.cmx *.ppi *.ppo *.o *.exe

.SUFFIXES: .ml .mli .cmx .cmo .cmi

.ml.cmo:
	$(COMPILEI) $<

.mli.cmi:
	$(COMPILEI) $<

.ml.cmx:
	$(COMPILE) $<

include .depend

