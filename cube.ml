(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Util;
open Term;
open Expression;
open Lambda;
open Reduction;
open Redexes;


(* marks : redexes -> position list *)
value rec marks = fun
  [ MRef _               -> []
  | MAbs e               -> sons F (marks e)
  | MApp True (MAbs e) r -> [root :: (sons (A L) (sons F (marks e)))]
                                  @ (sons (A R) (marks r))
  | MApp True _ _        -> error "Only redexes ought to be marked"
  | MApp False l r       -> (sons (A L) (marks l))
                          @ (sons (A R) (marks r))
  ]
;
(* (marks (mark "[y](#[x1,x2](#[z]x2 y) y)") = [[F]; [F; A L; F; F]]); *)

(* The marked translation of a term *)
value rec void = fun
 [ Ref k   -> MRef k
 | Abs e   -> MAbs (void e)
 | App l r -> MApp False (void l) (void r)
 ]
;
value is_void e = (marks e = [])
;
(* The reverse translation *)
value rec unmark = fun
 [ MRef k     -> Ref k  
 | MAbs e     -> Abs (unmark e)
 | MApp _ l r -> App (unmark l) (unmark r)
 ]
;
value erase e = void(unmark e)
;
value is_marking (e,t) = unmark(e)=t
;
(* Two marked terms e and e' are compatible iff they have the same unmarking *)
value compatible(e,e') = unmark(e)=unmark(e')
;
(* Adding a mark to a marked term e at a redex position u *)
value add_mark e u  = mark_rec (e,u) 
where rec mark_rec = fun
 [ (MApp _ (MAbs _ as l) r , []) -> MApp True l r
 | (MAbs e , [F::u])             -> MAbs (mark_rec(e,u))
 | (MApp b l r , [(A L)::u])     -> MApp b (mark_rec(l,u)) r
 | (MApp b l r , [(A R)::u])     -> MApp b l (mark_rec(r,u))
 | _ -> error "There is no redex at this position"
 ]
;
(* marking a set of redexes in a term *)
value marking t = List.fold_left add_mark (void t)
;
(* The singleton set of redexes [u] in term t *)
value singleton(t,u) = add_mark (void t) u
;
(* For every correctly marked e, i.e. such that marks(e) succeeds,
   e = marking (unmark e) (marks e) *)

(* All redex positions in a marked term *)
value rec reds = fun
 [ MRef _            -> []
 | MAbs e            -> sons F (reds e)
 | MApp _ (MAbs e) r -> [root :: (sons (A L) (sons F (reds e)))]
                        @ (sons (A R) (reds r))
 | MApp _ l r        -> (sons (A L) (reds l)) @ (sons (A R) (reds r))
 ]
;
(* The unmarked redexes *)
value rec umr = fun
 [ MRef _                -> []
 | MAbs e                -> sons F (umr e)
 | MApp False (MAbs e) r -> [root :: (sons (A L) (sons F (umr e)))]
                                  @ (sons (A R) (umr r))
 | MApp _ l r            -> (sons (A L) (umr l)) @ (sons (A R) (umr r))
 ]
;
(* Note. For a correctly marked term e, 
   reds(e) is the union of marks(e) and umr(e). *)

(* The Boolean algebra of mutually compatible redexes *)

(* The fully marked term: all redexes are marked *)
value full t = marking t (redexes t)
;
value rec sub = fun
  [ (MRef k,MRef k') -> if k=k' then True
                        else error "Incompatible terms" 
 | (MAbs e,MAbs e') -> sub(e,e')
 | (MApp True e f,MApp False e' f') -> 
          if compatible(e,e') && compatible(f,f') then False
          else error "Incompatible terms"
 | (MApp _ e f,MApp _ e' f') -> sub(e,e') && sub(f,f') 
 | _ -> error "Incompatible terms"
 ]
;
(* sub(e,f)=True iff marks(e) is a subset of marks(f) *)

(* For every set e compatible with term t, sub(e,full t). 
   Actually, the set of redexes compatible with a given term t has 
   the structure of a Boolean algebra, with ordering sub, minimal element 
   (void t), maximal element (full t), union union and intersection inter *)

value rec union = fun
 [ (MRef k,MRef k') -> if k=k' then MRef k
                       else error "Incompatible terms" 
 | (MAbs e,MAbs e') -> MAbs(union(e,e'))
 | (MApp False e f,MApp False e' f') ->
                     MApp False (union(e,e')) (union(f,f'))
 | (MApp _ e f,MApp _ e' f') ->
                     MApp True (union(e,e')) (union(f,f'))
 | _ -> error "Incompatible terms"
 ]
;
value rec inter = fun
 [ (MRef k,MRef k') -> if k=k' then MRef k
                       else error "Incompatible terms" 
 | (MAbs e,MAbs e') -> MAbs (inter(e,e'))
 | (MApp True e f,MApp True e' f') ->
                     MApp True (inter(e,e')) (inter(f,f'))
 | (MApp _ e f,MApp _ e' f') ->
                     MApp False (inter(e,e')) (inter(f,f'))
 | _ -> error "Incompatible terms"
 ]
;

(**************)
(* Derivation *)
(**************)

(* A pair (e,r) of compatible redex sets represents the reduction of e,
   and the residuals of its marked redexes, along one step of parallel
   reduction of all the redexes marked in r. The reduction is said to
   be singleton if marks(r) is singleton, empty if marks(r)=[]. *)

value rec derive d d' = match (d,d') with 
 [ (MRef k, MRef k')              -> if k=k' then MRef k
                                     else error "Incompatible terms" 
 | (MAbs e, MAbs e')              -> MAbs(derive e e')
 | (MApp _ (MAbs e) f, MApp True (MAbs e') f')
                                  -> let body = derive e e'
                                     and arg = derive f f'
                                     in msubst arg body
 | (MApp b e f, MApp False e' f') -> MApp b (derive e e') (derive f f')
 | _                              -> error "Incompatible terms"
 ]
;
(* infix \ used for derive below *)

(* Now the residuals of the redexes marks(vv) by the reduction which
   contracts the redexes marks(uu) are obtained as marks(vv\uu);
   the redexes created by this reduction are given by Umr(vv\uu). 
   We leave to the reader to verify that 
      (reduce t u) = let uu=mark(t) in unmark(uu\(add_mark uu u)),
   and that (redex t v) => (residuals t u v) = marks(vv\uu)
            where vv=singleton(t,v) and uu=singleton(t,u).
 
   We define parallel reduction of terms in a similar manner: *)

(* assuming is_marking(e,t) *)
value par_reduce t e = unmark (derive (void t) e)
;
value full_reduction t = par_reduce t (full t)
;
(* Gross iterates full reduction to a normal form if possible *)
value rec gross t = 
  if is_normal t then t else gross (full_reduction t)
;

(********************)
(* The Main Theorem *)
(********************)

(* We leave the reader check the following easy proposition *)
(* Proposition. For any compatible redexes sets U and V,
union(U,V)\U = V
U\union(U,V) = erase(U). *)

(* Furthermore: *)

(* Theorem. derive is increasing, in the sense that:
For every mutually compatible redex sets R1,R2,W :
sub(R2,R1) =>  W\R1 = (W\R2)\(R1\R2) *)

(* Corollary : the cube lemma (Levy) 
For every mutually compatible redex sets U,V,W :
(W\U)\(V\U) = (W\V)\(U\V).
Proof: Apply twice the theorem with union(U,V) for R1 and U (resp. V) for R2 *)

(* Note that we get the cube lemma directly. When we remove the marks on W we 
get the usual parallel moves lemma (Tait-Martin-Lof proof):

   Corollary ("Diamond lemma"): Parallel reduction is strongly confluent
   Corollary: Parallel reduction is confluent
   Corollary: Beta-reduction is confluent
   Corollary: The Church-Rosser property
   Corollary: The normal form of a term, when it exists, is unique 
*)


