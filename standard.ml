(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Util;
open Term;
open Expression;
open Lambda;
open Reduction;
open Redexes;
open Cube;
open Derivation;

(* seq_cut cuts a sequence into (s1,s2) with |s1|=k *)
value seq_cut s k = init_rec (k,s,[]) 
where rec init_rec = fun
  [ (0,rest,acc)      -> (List.rev acc,rest)
  | (k,[x::rest],acc) -> init_rec (k-1,rest,[x::acc])
  | _                 -> error "Sequence too short for seq_cut"
  ]
;
(* initial prefix of length k of a sequence, and corresponding suffix *)
value initial s k = fst(seq_cut s k)
and final s k = snd(seq_cut s k)
;
(* sublist [si ... sj] *)
value sublist s i j = initial (final s (i-1)) (j-i+1)
;
(* The set of redexes vv contributes_fully_at step k to derivation d *)
value contributes_fully_at vv d k = 
  let s = sequence d in
  let sk = initial s (k-1)
  and uk = List.nth s k in
  let rv = res vv sk in
  sub(rv,uk) &&    (* All residuals of vv are contracted at step k *)
  not(is_void rv)  (* Of which there is at least one *)
;
value contributes_fully vv d = 
  List.exists (contributes_fully_at vv d) (range (der_length d))
;
(* Remark. 
  By definition, vv has no residual by d if it contributes fully to it:
  (contributes_fully vv d) => is_void(res_der vv d).
 *)


(****************************)
(* Induction on derivations *)
(****************************)

(* Let d=Der(t,s), d'=Der(t',s') be two derivations of the same length.
   We say that d' is a contraction of d iff d' is the residual of d 
   by some set vv which contributes fully to d. 
   We thus get equiv d (Der(t,[vv::s']), with vv non-void. *)
value contraction d vv = 
  if (contributes_fully vv d) then der_res d vv
  else error "Redex set does not fully contribute to derivation"
;
(* Theorem. The contraction map is contracting, in the sense that
there is no infinite sequence d1, d2, ... dn, with dn a contraction
of dn-1. 

Proof.
By contradiction. Assume such an infinite sequence, issued from d1 of length
n. There must be an infinite sequence [(r1,k1); (r2,k2); ...] such that
(contributes_fully ri di ki) with 1<=ki<=n. Let k be maximum such that k=ki
infinitely often. After some finite j, there is no kj>k. The infinite
subsequence [ri | i>=k & (contributes_fully ri di k)] defines an infinite
non-empty development of (List.nth (sequence dj) k). Contradiction. *)

(* Corollary. Any non-empty derivation is contractable (e.g. by the redexes of
   its first non-void step). Thus we may contract any derivation d=Der(t,s) 
   into an empty derivation by a sequence r=[r1; rr2; ... rn] of non void 
   steps, with equiv d (Der(t,r)). 
   This process always terminates by the theorem above. *)

(* Corollary. Derivation induction.
Any property of derivations which is true of empty derivations and
true of a derivation when true of its contractions is universally true. *)

(* Remark. This is the way to express the finite developments property
   as an induction principle for derivations. We may wonder whether we could
   get a more general principle by generalising the definition of
   contributes_fully. For instance, we might think it could be enough to
   replace contributes_fully by contributes below.
   But this is not the case, as the following counter-example, a variation on
   (Y I), shows. We take d to be the unit derivation contracting the redex set
       uu = mark "(#[z]z ([x](#[z]z (x x)) [x](#[z]z (x x))))>>",
   and vv = mark "(#[z]z (#[x]([z]z (x x)) [x]([z]z (x x))))>>".
   Note that vv and uu share a common redex position. But uu\vv = uu. *) 

(* The set of redexes vv contributes to derivation d at step k
   if some residual of vv is contracted at step k *)
value contributes_at vv d k = 
  let s = sequence d in
  let sk = initial s (k-1)
  and uk = List.nth s (k-1) in
  let rv = res vv sk in not(is_void(inter(rv,uk)))
;
value contributes vv d = 
  List.exists (contributes_at vv d) (range (der_length d))
;

(*******************)
(* Standardisation *)
(*******************)

(* We first start with a technical lemma on residuals of outer redexes. *)

(* Lemma 1. Preservation of outer redex positions.
   Let u and v be redex positions in term t, with (outer u v). 
   We consider the reduction from t to s=(reduce t v). We have:
1. (residuals t v u) = [u]
2. If w is a created redex position in t, then (outer u w)
3. For every position w of t, with (outer u w), we have (outer u w')
   for every w' in (residuals t v w).

   Lemma 2. Same as lemma 1, with one step of parallel derivation vv,
such that (outer u v) for every v in (marks vv).

Proofs. Left to the reader. *)

(* Definition. Standard derivation. *)

(* Let d be a single derivation: d=sder(t,s) of length n:
   t -s1-> t1 -s2-> ... -sn-> tn
We say that d is nonstandard at step j relatively to step i<j
when sj is a residual of some redex position u in term ti,
with (outer u si). More positively: *)

value rec check tv = fun
 [ []   -> True
 | [v::s] -> let (_,vv) = tv in 
             not(List.mem v vv) &&
             check (red_and_res tv v) s
 ]
;
value rec is_standard_seq t = fun
 [ []   -> True
 | [u::s] -> 
   let vv = List.filter (fun v -> outer v u) (redexes t) in
   let tv = red_and_res (t,vv) u in
   (check tv s) && (is_standard_seq (fst tv) s)
 ]
;

(* An equivalent slightly faster definition

value is_standard_seq t s = 
  let n=List.length s in
  List.for_all standard_relative (range n)
  where standard_relative i = 
    let ti=reduce_seq t (initial s (i-1)) 
    and si=List.nth s (i-1)
    in List.for_all outer_redex (redexes ti)
       where outer_redex u =     
         not(outer u si) 
         || not(List.exists reducing_step (interval (i+1) n)
                where reducing_step j = 
                   is_residual ti [u] (sublist s i (j-1)) (List.nth s (j-1)));
*)
 
value is_standard = fun
  [ Der(t,s) -> is_standard_seq t (singles s) ]
;
(* Examples.
value t = <<([x](^I x) (^I ^K))>>;

value u1 = [A L; F]  (* -> t1 = (I (I K)) *)
and u2 = [A R]       (* -> t2 = (I K)     *)
and u3 = [];         (* -> t3 = K         *)

is_standard_seq t [u1;u2] = True;
is_standard_seq t [u1;u2;u3] = False;

assert (let t = <<(^Fact ^Three)>> in 
        is_standard_seq t (normal_sequence t));
*)

(* Let d be any derivation. We define a single derivation standard(d) 
as follows. *)

value is_relevant d u = 
  let t = start_term d in contributes (singleton(t,u)) d
;
value rec standard_seq d = 
  if is_empty d then []
  else let t = start_term d in
       let relevant_redexes = List.filter (is_relevant d) (redexes t) in 
       let out = List.hd (relevant_redexes) in
       [ out :: standard_seq (single_res d out) ]
;
(* (redexes t) lists the redexes of t in leftmost-outermost order,
   and this property is preserved by filter; thus out is the 
   lefmost-outermost redex of t contributing to d. *)

(* Lemma. For every d, s=standard_seq d is well-defined, and is_standard_seq s.
Proof. Using lemma 2 above, in the case d non-empty, the redex position out 
stays as unique residual, until it is reduced. We may therefore use derivation 
induction, since (single_res d out) is a contraction of d, proving the 
termination of standard_seq. By construction, we get a standard derivation
sequence. *)

value standard d = sder (start_term d,standard_seq d)
;
(* Standardisation Theorem. 
 Every derivation is equivalent to a unique standard derivation.
 Proof. Consider standard(d). By the lemma above, it is a standard derivation.
   The construction ends when the residual of d by standard(d) is empty, and
   thus equiv (standard d) d. We leave it to the reader to show unicity, i.e.:
   Let d' equiv d, such that is_standard(d'). Then d'=standard(d).
*)

(* Beware. It is not true that (equiv d' d) implies that they have the same
set of relevant redexes. Consider:
value t = <<([x]^I (^I ^I))>> 
and e = mark "(#[x][u]u ([u]u [u]u))"
and e' = mark "(#[x][u]u (#[u]u [u]u))";
value d=Der(t,[e]) and d'=Der(t,[e']);
d and d' are two equivalent derivations leading from t to its normal form.
But the set of redexes of t relevant to d (i.e. marks(e)) is different from 
the set of redexes of t relevant to dd' (i.e. marks(e')). 
What matters is that equivalent derivations have the same outermost relevant
redex, in this case the root position of t. Note that here d=standard(d').
*)

(* We may now justify the normal strategy used by function nf: *)


(**************************************)
(* Correctness of the normal strategy *)
(**************************************)

(* normal derivation issued from term t *)
value normal t = sder(t,normal_sequence t)
;
(* Whenever (normal t) is defined, it is standard, since the leftmost-outermost
redex is relevant to any derivation leading to the normal form, using lemma 2.
Thus if a term possesses a normal form, the normal derivation leads to it:

Theorem. Correctness of the normal strategy.
If t possesses a normal form nt, normal(t) is defined and leads to nt. *)


(* Actually, any quasi-normal strategy is correct, where a quasi-normal
   derivation sequence issued from a term reduces ultimately its 
   leftmost-outermost redex. For instance, the Gross full-reduction
   strategy is correct. *)

