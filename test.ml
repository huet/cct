(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2004 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

(*****  Test Suite  *****)

(*****  1.  Syntax  *****)

#load "util.cmo"; 
#load "term.cmo"; 
#load "expression.cmo"; 
#load "lambda.cmo"; 

open Util;
open Term;
open Expression;
open Lambda;

(* Uncomment for "machine language" printing of terms 
open Format;

(* machine language format *)
value rec print_term t = printer True t
where rec printer flag = fun
 [ Ref n   -> printf "%s" (string_of_int n)
 | App u v -> 
   let pr_app u v = do { printer False u; printf " "; printer True v }
   in if flag then do { printf "("; pr_app u v; printf ")" }
      else pr_app u v
 | Abs t   -> do { printf "^"; print_term t }
 ];
*)

#install_printer print_term; 

(* alpha conversion is implicit *)
assert ( <<([x,y](x (y [x]x))  [u](u u)  [v,w]v)>>
       = <<([x0,x1](x0 (x1 [x2]x2)) [x0](x0 x0) [x0,x1]x0)>>);

assert(match <<[u][z,y][x,x'](x x' y z u)>> with
    [ Abs (Abs (Abs l)) -> expression ["y";"z"] l | _ -> fail() ]
    = <:expr<[x2][x3](x2 x3 y z u0)>>);

assert(let f = <<[x][y](y [z](x z))>>
       and x = <<[z]z>> in
       match f with [ Abs body -> subst x body | _ -> fail() ]
       = << [y](y [z]([u]u z)) >>);

(*****  2.  Examples  *****)

#load "examples.cmo"; 
#load "arith.cmo"; 
#load "arith1.cmo"; 
#load "recursion.cmo"; 

open Examples;
open Arith;
open Arith1;
open Recursion;

type quadtree = [ Q | Q4 of (quadtree * quadtree * quadtree * quadtree) ];
value quad n = eval_nat (fun t -> Q4(t,t,t,t)) Q (church n);

assert (quad 3 = 
Q4
 (Q4
   (Q4 (Q, Q, Q, Q), Q4 (Q, Q, Q, Q), Q4 (Q, Q, Q, Q),
   Q4 (Q, Q, Q, Q)),
 Q4
  (Q4 (Q, Q, Q, Q), Q4 (Q, Q, Q, Q), Q4 (Q, Q, Q, Q),
  Q4 (Q, Q, Q, Q)),
 Q4
  (Q4 (Q, Q, Q, Q), Q4 (Q, Q, Q, Q), Q4 (Q, Q, Q, Q),
  Q4 (Q, Q, Q, Q)),
 Q4
  (Q4 (Q, Q, Q, Q), Q4 (Q, Q, Q, Q), Q4 (Q, Q, Q, Q),
  Q4 (Q, Q, Q, Q))));

assert (normal_nat<<(!Add !Two !Two)>> = 4); 

assert (normal_nat<<(!Mult !Five !Five)>> = 25); 

assert (normal_nat<<(!Exp !Three !Five)>> = 243); 

assert (normal_nat<<(!Fact !Three)>> = 6);

assert (normal_nat<<(!Fib !Five)>> = 8);

assert (normal_nat<<(!Pred !Five)>> =4);

assert (normal_nat<<(!Ack !Two)>> = 7); 

assert (let _L = list[1;2;3] in normal_nat<<(!Length !L)>>=3);

assert (normal_list<<(!Range !Four)>> = [1; 2; 3; 4]);
 
assert (normal_list<<(!Map !Fact (!Range !Four))>> = [1; 2; 6; 24]);

assert (normal_nat<<(!Fact' !Three)>> = 6);

assert (normal_list<<(!Fst (!Partition (!Geq !Two) (!Range !Four)))>> = [1; 2]);

assert (let _L=list[3;2;5;1] in normal_list<<(!Quicksort !L)>>
 = [1; 2; 3; 5]); (* (27s) *)

assert (let _L=list[3;2;5;1] in applicative_list<<(!Quicksort !L)>>
 = [1; 2; 3; 5]); (* (2s) *)

assert (let _One'= <<(!Succ' !Zero')>> in nf<<(!Pred' !One')>> = _Zero'); 

assert (normal_nat_rec<<(!Plus !Nat2 !Nat2)>> = 4);

assert (normal_nat_rec<<(!Times !Nat5 !Nat3)>> = 15);

assert (normal_nat_rec<<(!Power !Nat3 !Nat3)>> = 27);

assert (normal_nat_rec<<(!Fact'' !Nat3)>> = 6);

assert (godel _Delta = 88);
 
assert (godel _Omega = 31328 && _Omega = ungodel 31328);

(* Caution : truncation provokes Error: 
   Integer literal exceeds the range of representable integers of type int

assert (godel _Fix = 6941718342796165477078794502929179108365127687513804648);
*)
assert (kleene _Delta = church (godel _Delta));


(*****  3.  Beta reduction  *****)

#load "reduction.cmo"; 

open Reduction;

assert (dom <<[x](x [y](x y))>> = 
[[]; [F]; [F; A L]; [F; A R]; [F; A R; F]; [F; A R; F; A L]; [F; A R; F; A R]]);

assert (red <<(!Omega (!Succ !Zero))>> <<(!Omega !One)>>);

assert (red <<(!Fix !K)>> <<(!K (!Fix !K))>>);

assert (conv <<(!Y !K)>> <<(!K (!Y !K))>>);

assert (compute_nat (normalise <<(!Add !Two !Two)>>) = 4);

assert (List.length(normal_sequence <<(!Fact'' !Three)>>) = 191);

assert (List.length(normal_sequence <<(!Fact !Three)>>) = 325);

assert ((let t = <<[u]([z](z z) [y]([x]y u))>> 
         and seq = [[F]; [F]] 
         and r = [F; A R; F] in (* ________ *)
         trace_residuals (t,[r]) seq) =
         (<<[u]([x,y]([w]y u) u)>>,[[F]; [F; A L; F; F]])
          (*   _________________    ^
                     ________            ^  *)
       );  


(*****  4.  Derivations  *****)

#load "redexes.cmo"; 
open Redexes;
#install_printer print_marked;

assert (msubst (MAbs (MRef 0)) (MAbs (MApp False (MRef 0) (MRef 1))) = 
               (MAbs (MApp False (MRef 0) (MAbs (MRef 0)))));


#load "cube.cmo"; 
open Cube;

assert (marks (mark "[y](#[x1,x2](#[z]x2 y) y)") = [[F]; [F; A L; F; F]]);

value r1 = mark "[y](#[x]x ([x]x y))" 
and r2 = mark "[y]([x]x (#[x]x y))"
and e = mark "[y](#[x]x y)";
assert (derive r1 r2 = e && derive r2 r1 = e);

#load "derivation.cmo"; 
open Derivation;

value t = <<[x](!I (!I x))>>;
value d1 = Der(t,[r1]) 
and d2 = Der(t,[r2]);
assert (end_term d1 = end_term d2);
assert (not (equiv d1 d2));

value t = <<[u1]([z](z z) [y]([x]y u1))>> 
and s = [[F; A R; F]; [F]];
assert (sder(t,s) = Der(t, List.map mark 
  [ "[x]([y](y y) [z](#[u]z x))"; "[x](#[y](y y) [z]z)" ]));


(*****  5.  Developments  *****)

#load "develop.cmo"; 
open Develop;

value e1 = mark "[u]([z](z z) [y](#[x]y u))"
and r1 = mark "[u](#[z](z z) [y]([x]y u))";
value e2 = derive e1 r1;
assert (e2 = mark "[u]([x1](#[x2]x1 u) [x1](#[x2]x1 u))");

value r2 = mark "[u](#[x1]([x2]x1 u) [x1]([x2]x1 u))";
value e3 = derive e2 r2;
assert (e3 = mark "[u](#[x1,x2](#[x3]x2 u) u)");

value d1 = Res(e1,[r1;r2]);
assert (e3=end_res d1);
assert (not (is_development d1));

value e'1 = mark "[u](#[z](z z) [y](#[x]y u))";
value e'2 = derive e'1 r1;
assert (e'2 = mark "[u]([x1](#[x2]x1 u) [x1](#[x2]x1 u))");

value r'2 = mark "[u]([x1](#[x2]x1 u) [x1]([x2]x1 u))";
value e'3 = derive e'2 r'2;
assert (e'3 = mark "[u]([x1]x1 [x1](#[x2]x1 u))");

value d2=Res(e'1,[r1; r'2]);
assert (is_development d2);
assert (not (is_complete_development d2));

value d3=Res(e'1,[r1; r'2; e'3]);
assert (is_development d3);
assert (is_complete_development d3);
 
value e = mark "[u](#[z](z z z) [y](#[x](x y) u))";
assert (bound e = 12);
assert (decreasing (set_weight e));


(*****  6.  Standardisation  *****)

#load "standard.cmo"; 
open Standard;

value t = <<([x](!I x) (!I !K))>>;
value u1 = [A L; F]  (* -> t1 = (I (I K)) *)
and   u2 = [A R]     (* -> t2 = (I K)     *)
and   u3 = [];       (* -> t3 = K         *)
assert (is_standard_seq t [u1;u2]);
assert (not (is_standard_seq t [u1;u2;u3]));

assert (let t = <<(!Fact !Two)>> in 
        is_standard_seq t (normal_sequence t)); (* costly *)

value t = <<([x]!I (!I !I))>> 
and e = mark "(#[x][u]u ([u]u [u]u))"
and e' = mark "(#[x][u]u (#[u]u [u]u))";
value d = Der(t,[e]) and d' = Der(t,[e']);
assert (equiv d d');
assert (marks e = [[]] && marks e' = [[]; [A R]]);
assert (let u = [A R] in (is_relevant d' u) && not (is_relevant d u));

(*****  7.  Separation  *****)

#load "eta.cmo"; 
#load "bohm.cmo"; 

open Eta;
open Bohm;

value loop = <<(!Y [x,y](y x))>>;
value bt1 = approximate loop;
value bt2 = match bt1 with 
  [ Hnf (n,_,[Future t]) ->  approx [n] t
  | _ -> fail () ];
assert (bt1=bt2);
(* loop has the infinite Bohm tree b solution of b=[x](x b) :
   [x1](x1 [x2](x2 [x3](x3 [x4](x4 ... ))))  *)

value bt1 = approximate _J;
value bt2 = match bt1 with
  [ Hnf (2,_,[Future bt]) -> approx [2] bt 
  | _ -> fail ()];
value bt3 = match bt2 with
  [ Hnf (1,_,[Future bt]) -> approx [1;2] bt
  | _ -> fail ()];
value bt4 = match bt3 with
  [ Hnf (1,_,[Future bt]) -> approx [1;1;2] bt
  | _ -> fail ()];
assert (bt4=bt3);
(* _J has the infinite Bohm tree  [x1,x2](x1 [x3](x2 [x4](x3 ... ))) *)

assert (let t = <<[x](x [y](x y))>> in
   (access t (Path []) = let s = (Abs (App (Ref 1) (Ref 0))) in
  (Hnf(1, Index (0,1), [Future s]), True, []))
&& (access t (Path [1]) = 
  (Hnf(1, Index (1,1), [Future (Ref 0)]), True, [Present 1]))
&& (access t (Path [1;1]) = 
  (Hnf(0, Index (1,1), []), False, [Present 1; Present 1]))
&& (access t (Path [1;2]) =
  (Hnf(0, Index (1,2), []), False, [Present 1; Present 1])));

assert ((tuple 3 = <<[x1,x2,x3,x4](x4 x1 x2 x3)>>)
     && (tuple 2 = _Pair)
     && (tuple 0 = _I));

assert ((pi 1 1 = _I) && (pi 1 2 = _True) && (pi 2 2 = _False));

assert (k_ 1 = _K);

assert (project _Y (semi_sep _Y) = _I); 

assert (let context = separate(_I,_Pair) (Path [])
        in separates context (_I,_Pair));

assert (let t = <<[u](u !I (u !I !I))>>
        and s = <<[u](u !I (u u !I))>>
        and p = Path [2;1] in
        let context = separate (t,s) p
        in separates context (t,s));

assert (let t  = <<[u,v](v [w](u w))>>
        and s = <<[u,v](v [w](u u))>>
        and p = Path[1;1] in
        let context = separate (t,s) p in
        separates context (t,s));

assert (let t = <<[u](u u)>>
        and s = <<[u](u [v]v)>>
        and p = Path[1] in
        let context = separate (t,s) p
        in separates context (t,s));

assert (let t = <<[u](u u)>>
        and s = <<[u](u [v,w](w v))>>
        and p = Path[1] in
        let context = separate (t,s) p in
        separates context (t,s));

assert (let t = <<[u](u (u u u) u)>>
        and s = <<[u](u (u u [u,v,w](w u v)) u)>>
        and p = Path[1;2] in
        let context = separate (t,s) p in
        separates context (t,s));

assert (let t = <<[u,v](u [w](u w))>>
        and s = <<[u,v](u [w](u u))>>
        and p = Path[1;1] in
        let context = separate (t,s) p in 
        separates context (t,s));

assert (let t = <<[x](x [y](x y))>>
        and s = <<[x,u](x [y,z,t](x y z z) u)>> 
        and p = Path[1;3] in
        let context = separate (t,s) p in
        separates context (t,s));

assert (let context = separate (_S,_Fix) (Path []) in
        separates context (_S,_Fix));

assert (let context = separate (_Fix,_S) (Path []) in
        separates context (_Fix,_S));

assert (let t = <<[x1,x2,x3](x1 !I)>> in
        let context = separate (t,_Fix) (Path []) in
        separates context (t,_Fix));

assert (let t = <<[u](u [v](v (u u u)))>>
        and s = <<[u](u [v](v (u u [u,v,w](w u v))))>>
        and p = Path[1;1;2] in
        let context = separate (t,s) p in
        separates context (t,s));

assert (bohm(_Delta,<<[x,y](x y y)>>) =
        [ tuple 2; tuple 3; pi 1 2; pi 1 1; pi 1 1; pi 4 5; pi 5 5 ]);

assert (bohm(_I,_Pair) = [ pi 3 4; pi 1 1; pi 4 4 ]);

assert (bohm(_Zero,_One) = [ pi 3 3; pi 1 2 ]);

assert (bohm(_I,_Y) = [ pi 2 3; pi 1 2; pi 1 2; pi 1 1 ]);

assert (bohm(_S,_Fix) = [ pi 3 4; pi 1 2; pi 1 2; pi 1 2; pi 1 1 ]);

assert (bohm(<<[u,v](v [w](u w))>>,<<[u,v](v [w](u u))>>) =
        [ tuple 1; pi 1 1; tuple 2; pi 1 1; pi 1 1; pi 4 4; pi 3 4 ]);

assert (bohm(<<[u](u (u u u) u)>>,<<[u](u (u u [u,v,w](w u v)) u)>>) =
  [ tuple 3; pi 1 1; pi 1 3; pi 1 1; pi 2 3; pi 1 1; pi 1 1; pi 5 5; pi 4 5 ]);

assert (bohm_check (<<[x](x x x)>>,<<[x](x x)>>));
assert (bohm_check (<<[x](x x)>>,<<[x](x x x)>>));
assert (bohm_check (<<[x,y](x x x x)>>,<<[x](x x)>>));
assert (bohm_check (<<[x](x x)>>,<<[x,y](x x x x)>>));
