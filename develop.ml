(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Util;
open Term;
open Expression;
open Lambda;
open Reduction;
open Redexes;
open Cube;
open Derivation;

(************************)
(* Residual derivations *)
(************************)

(* Just like derivations, but issued from a set of redexes. *)

type residuals = [ Res of (redexes * sequence) ]
;
(* Res(e,[r1;r2; ... rn]) represents the derivation which contracts
in parallel successively the sets of redexes r1, r2, ..., rn, starting
from the term unmark(e), and in which we are interested by the residuals
of all redexes marked in e. *)

(* The initial and final redex sets *)
value begin_res = fun [ Res(e,_) -> e ]
and end_res = fun [ Res(e,seq) ->  List.fold_left derive e seq ]
;
(* complete residual derivation *)
value is_complete d = is_void (end_res d)
;
(* complete : unit complete derivation of all given redexes *)
value complete e = Res(e,[e])
;
(* Example: The previously considered residual computation:  
             e1 -r1-> e2 -r2-> e3
value e1 = mark "[u]([z](z z) [y](#[x]y u))"
and r1 = mark "[u](#[z](z z) [y]([x]y u))";
value e2 = derive e1 r1;
 (* = mark "[u]([x1](#[x2]x1 u) [x1](#[x2]x1 u))" *)
value r2 = mark "[u](#[x1]([x2]x1 u) [x1]([x2]x1 u))";
value e3 = derive e2 r2;
 (* = mark "[u](#[x1,x2](#[x3]x2 u) u)" *)
let d1=Res(e1,[r1;r2]) in e3=end_res(d1); (* True *)
*)

(* A residual derivation d is said to be consistent iff end_res(d) succeeds.
   This is independent of the marks in begin_res(d). *) 

(* The empty residual derivation issued from redex set e *)
value id_der e = Res(e,[])
;
(* residual derivations composition *)
value comp_der d1 d2 = 
  match d1 with [ Res(t1,s1) ->
  match d2 with [ Res(t2,s2) ->
    if t2=end_res d1 then Res(t1,s1 @ s2)
    else error "Derivations not composable" ]]
;
(* Another characterisation of derivation equivalence.
   equiv (Der(t,s)) (Der(t,s') iff for every subset r of redexes of t
   we have end_res(d)=end_res(d'), with d=Res(e,s) and d'=Res(e,s'),
   where e=marking t r. *)


(******************)
(* Developements *)
(******************)

(* A residual derivation  e1 -r1-> e2 -r2-> e3 -> ...
 is a development of t1 iff for every i we have sub(ri,ei).
 The development is complete at step n if marks(en+1)=[]. 
 For instance, we get a complete development at step 1 when r1=e1. 
 The development is empty at step n if is_void(rn). *)

value is_development = fun
 [ Res(u,s) -> der u s
   where rec der u = fun
    [ []   -> True
    | [v::s] -> sub(v,u) && der (derive u v) s
    ]]
;
value is_complete_development d = 
  is_development d && is_complete d
;
(* Note that for every e, is_complete_development(complete e). *)

(* With the example above, is_development(d1)=False, since the redex in r1 
is not marked in e1. But, with
value e'1 = mark "[u](#[z](z z) [y](#[x]y u))"
(* and r1 as above, we get: *)
value e'2 = derive e'1 r1;
   (* mark "[u]([x1](#[x2]x1 u) [x1](#[x2]x1 u))" *)
value r'2 = mark "[u]([x1](#[x2]x1 u1) [x1]([x2]x1 u1))" 
value e'3 = derive e'2 r'2;
 (* mark "[u]([x1]x1 [x1](#[x2]x1 u))" *)
value d2=Res(e'1,[r1; r'2])
and d3=Res(e'1,[r1; r'2; e'3]);
We get is_development(d2) and is_development(d3), but only 
is_complete_development(d3). *)

(* Proposition. If d=Res(e,s) is a development, then 
   with t=unmark(e), we have Der(t,s) leq Der(t,[e]). 
   If d is complete, we have Der(t,s) equiv Der(t,[e]). *)

(* The finite developments theorem *)
(* Theorem. Every development is ultimately complete or empty. *)

(* In other words, if we do not allow trivial empty steps, every
   development is finite. For this reason, this is usually called
   the finite developments theorem. In particular, any development
   may be completed into a complete development.
   The theorem shows that we may reduce a set of redexes by sequences
   of beta conversions of their mutual residuals in any order.
   It gives another proof of confluence, using local confluence *)

(* Proof of the finite development theorem *)

(* We first define a structure of weighted marked terms *)

type weighted =
 [ WRef of int
 | WAbs of int and weighted
 | WApp of bool and weighted and weighted
 ]
;
(* We weight a term by adding the weight of its variables *)
value rec weight_in free_weights = fun
 [ WRef n       -> List.nth free_weights n
 | WAbs w t     -> weight_in [w::free_weights] t
 | WApp _ t1 t2 -> 
   (weight_in free_weights t1) + (weight_in free_weights t2)
 ]
;
value weight = weight_in []
;
(* All notions of substitution and reduction extend to weighted terms *)

value wlift n = 
let rec lift_rec k = lift_k
where rec lift_k = fun
  [ WRef i       -> if i<k then WRef i else WRef (n+i)
  | WAbs w t     -> WAbs w (lift_rec (k+1) t)
  | WApp b t1 t2 -> WApp b (lift_k t1) (lift_k t2)
  ] in lift_rec 0
;
value wsubst wt = 
let rec subst_lam n = subst_n
where rec subst_n = fun
  [  WRef k       -> if k=n then wlift n wt
                     else if k<n then WRef k else WRef (k-1)
  |  WAbs w t     -> WAbs w (subst_lam (n+1) t)
  |  WApp b t1 t2 -> WApp b (subst_n t1) (subst_n t2)
  ]
in subst_lam 0
;
value rec wderive d d' = match (d,d') with 
  [ (WRef k, MRef k')   -> if k=k' then WRef k
                           else error "Incompatible terms" 
  | (WAbs w t, MAbs t') -> WAbs w (wderive t t')
  | (WApp _ (WAbs _ t) u, MApp True (MAbs t') u') -> 
           let body = wderive t t'
           and arg = wderive u u' in wsubst arg body
  | (WApp b t u, MApp False t' u') -> WApp b (wderive t t') (wderive u u')
  | _ -> error "Incompatible terms"
  ]
;
value rec forget_weights = fun
  [ WRef i       -> MRef i
  | WAbs _ t     -> MAbs (forget_weights t)
  | WApp b t1 t2 -> MApp b (forget_weights t1) (forget_weights t2)
  ]
;

(* Thus (forget_weight (wderive t r)) = derive (forget_weight t) r.  *)
 
(* Substitution Lemma.
Let weights be a list of positive integers of length n, wt (resp. ws) be a 
weighted term valid at depth n+1 (resp. n), and wq=(wsubst ws wt). 
Then for every w>=(weight_in weights ws), we have:
weight_in [w::weights] wt >= weight_in weights wq.
Proof: Induction on wt. *)

(* Corollary.
Under the same conditions, with wp=WApp b (WAbs w wt) ws, we have:
      weight_in weights wp > weight_in weights wq. 
Proof: since weight_in weights ws>0. *)

(* A weighted term is said to be decreasing iff
   for every of its marked redex (#[x:w]f t) we have w>=weight(t)  *)

value rec decr_in free_weights = fun
 [ WRef _     -> True
 | WAbs w t   -> decr_in [w::free_weights] t
 | WApp False f t -> (decr_in free_weights f) &&
                     (decr_in free_weights t)
 | WApp True (WAbs w _ as f) t -> 
                      (decr_in free_weights f) &&
                      (decr_in free_weights t) &&
                      w>=weight_in free_weights t
 | _ -> error "Marked non-redex"
 ]
;
value decreasing = decr_in []
;
(* We shall now show that to every marked closed term, there exists a 
   decreasing weighted term with the same term structure. *)

value rec set_weight_in free_weights w = fun
 [ MRef n -> (WRef n, List.nth free_weights n)
 | MAbs t -> let (t',w') = set_weight_in [w::free_weights] 1 t
              in (WAbs w t',w')
 | MApp b t t' ->
   let (t2,p) = set_weight_in free_weights 1 t' in
   let (t1,n) = set_weight_in free_weights (if b then p else 1) t in
   (WApp b t1 t2, n+p)
 ]
and set_weight t = let (wt,_) = set_weight_in [] 1 t in wt
;
value bound x = weight (set_weight x)
;
(* Note. For every w, weights and t: (set_weight_in weights w t) = (wt,p) 
   with p=(weight_in weights wt). *)

(* Example. 
value e = mark "[u](#[z](z z z) [y](#[x](x y) u))";
bound e;
=> 12
decreasing (set_weight e);
=> True
This is a general fact: *)

(* Proposition. 
For every closed weighted term wt, decreasing(set_weight wt). 
Proof: Induction on wt. *)

(* Decreasing preservation Lemma.
Decreasing-ness is preserved by substitution, in the following sense.
Let weights be a list of positive integers of length n, t (resp. t') be a 
weighted term valid at depth n+1 (resp. n), and t''=(wsubst t' t). 
Then for every w>=(weight_in weights t'), if (decr_in [w::weights] t) 
and (decr_in weights t'), then (decr_in weights t'').
Proof: Induction on t, using the Substitution Lemma. *)

(* Main Lemma. Decreasing terms have decreasing weights along developments.
That is, let weights be a list of positive integers of length n, 
wt be a decreasing weighted term valid at depth n, and uu be a set
of redexes marked in wt; i.e. sub(uu,forget_weight wt). Let ws=wderive(wt,uu). 
Then ws is a decreasing weighted term, with 
    (weight_in weights ws)<=(weight_in weights wt).
Furthermore, the inequality is strict when uu is not void.
Proof. Induction on (wt,uu).
*)

(* Corollary. 
Let e be a closed set of redexes. Every developement of e without empty steps
is of length less than bound(e).

Corollary. The finite developments theorem. 
(For a non-closed e, use its closure).
*)

(* Remark. The classical proof, due to Hyland and Barendregt, uses 
weights too, but different weights are assigned to the various
occurrences of a variable. Here, variables are uniformly weighted in
their binder. Thus weights may be thought of as types. *)
