(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Util;
open Term;
open Expression;
open Lambda;

(* Positions *)

(*- sibling direction position -*)
type sibling = [ L | R ]
and direction = [ F | A of sibling ]
and position = list direction
;
(* root : the principal position at the root of a term *)
value root = ([]:position)
and sons (d:direction) = List.map (cons d)
and empty = ([]:list position)
;
(* father brother *)
value father = fun 
  [ [(_:direction):: pos] -> pos 
  | [] -> error "Root has no father"
  ]
and brother = fun
  [ [(A L)::pos] -> [(A R)::pos]
  | [(A R)::pos] -> [(A L)::pos]
  | _ -> error "Not the son of an application"
  ]
;
type comparison = 
  [ Eq | Less | Greater | Left | Right | Inconsistent ]
;
value rec compare = fun
  [ ([],[]) -> Eq
  | ([],_)  -> Less
  | (_,[])  -> Greater
  | ([(A s)::u],[(A s')::v]) -> match (s,s') with
     [ (L,R) -> Left
     | (R,L) -> Right
     | _   -> compare(u,v)
     ]
  | ([F::u],[F::v]) -> compare(u,v)
  | _ -> Inconsistent
  ]
;
(* disjoint positions *)
value disjoint u v = 
  let test=compare(u,v) in test=Left || test=Right
;
(* Position u is above or to the left of v *)
value outer u v = 
  let test=compare(u,v) in test=Less || test=Left
;
(* Proposition. outer is a strict total ordering on mutually consistent
   positions. *)

(* Maximal commun prefix of two consistent positions, with corresponding suffixes. *)
value factor = fact_prefix root
where rec fact_prefix w = fun
  [ ([F::u],[F::v])          -> fact_prefix [F::w] (u,v)
  | ([(A s)::u],[(A s')::v]) -> if s=s' then fact_prefix [(A s)::w] (u,v)
                                else (List.rev(w) , ([(A s)::u],[(A s')::v]))
  | ([],v)                   -> (List.rev(w) , ([],v))
  | (u,[])                   -> (List.rev(w) , (u,[]))
  | _                        -> error "Inconsistent positions"
  ]
;
(* Number of abstractions visited by an position *)
value transfer = trans 0 
  where rec trans n = fun
  [ [F::u] -> trans (n+1) u
  | [_::u] -> trans n u
  | []     -> n
  ]
;
(* dom : term -> position list *)
value rec dom = fun
  [ Ref m     -> [root]
  | Abs e     -> [root :: (List.map (cons F) (dom e))]
  | App e1 e2 -> [root :: (List.map (cons (A L)) (dom e1))] @
                          (List.map (cons (A R)) (dom e2))
  ]
;

(* dom<<[x](x [y](x y))>> = 
[[]; [F]; [F; A L]; [F; A R]; [F; A R; F]; [F; A R; F; A L]; [F; A R; F; A R]];
*)

(* subterm of term M at position u is subterm(M,u). *)
value rec subterm = fun
  [ (e,[])               -> e
  | (Abs(e),[F::u])      -> subterm(e,u)
  | (App e _,[(A L)::u]) -> subterm(e,u)
  | (App _ e,[(A R)::u]) -> subterm(e,u)
  | _                    -> error "Position not in domain"
  ]
;
(* access combines subterm and replace *)
value rec access = fun
  [ (e,[])              -> (e, fun e' -> e')
  | (Abs e,[F::u])      -> let (s, f) = access(e,u) 
                           in (s, fun e' -> Abs e')
  | (App l r,[A(L)::u]) -> let (s, f) = access(l,u) 
                           in (s, fun e' -> App e' r)
  | (App l r,[A(R)::u]) -> let (s, f) = access(r,u) 
                           in (s, fun e' -> App l e')
  | _                   -> error "Position not in domain"
  ]
;
(* Unlifting *)

exception Occurs_free;
(* This exception is raised when trying to unlift a non-lifted term *)

value unlift1 = unlift_check 0
    where rec unlift_check n = fun
      [ Ref i   -> if i=n then raise Occurs_free
                   else if i<n then Ref(i) else Ref(i-1)
      | Abs t   -> Abs (unlift_check (n+1) t)
      | App t u -> App (unlift_check n t) (unlift_check n u)
      ]
;
(* unlift1 (lift 1 M) = M *)

value unlift = iter unlift1
;
(* Equality of subterms *)
value rec eq_subterm t (u,v) =
  let (_,(u',v')) = factor(u,v)
  and tu = subterm(t,u)
  and tv = subterm(t,v) in
  try unlift (transfer u') tu = unlift (transfer v') tv
  with [ Occurs_free -> False ]
;

(******************)
(* Beta reduction *)
(******************)

(* u is a (beta) redex position in term t *)
value is_redex t u = match subterm(t,u) with
 [ App (Abs _) _ -> True
 | _             -> False
 ]
;
(* All redex positions in a term, in outer order *)
value rec redexes = fun
  [ Ref _         -> empty
  | Abs e         -> sons F (redexes e)
  | App (Abs e) d -> [root :: (sons (A L) (sons F (redexes e)))]
                     @ (sons (A R) (redexes d))
  | App g d       -> (sons (A L) (redexes g)) @ (sons (A R) (redexes d))
  ]
;
(* redexes t = List.filter (is_redex t) (dom t) *)

value is_normal t = (redexes t) = []
;
(* t[u<-s] = replace s (t,u) *)
value replace w = rep_rec
where rec rep_rec = fun
 [ (_,[])               -> w
 | (Abs e,[F::u])       -> Abs (rep_rec (e,u))
 | (App l r,[(A L)::u]) -> App (rep_rec (l,u)) r
 | (App l r,[(A R)::u]) -> App l (rep_rec (r,u))
 | _                    -> error "Position not in domain"
 ]
;
(****************************)
(* Reduction and conversion *)
(****************************)

(* Beta-reduction of term t at redex position u. *)
value reduce t u = match subterm(t,u) with
  [ App (Abs body) arg -> replace (subst arg body) (t,u)
  | _                  -> error "Position not a redex"
  ]
;

(* Reduce a sequence of redex positions *)
value reduce_seq = List.fold_left reduce
;
(* Reduction is reflexive-transitive closure of beta-reduction. *)

(* naive red *)
value rec red t u = (* depth-first *)
   u=t || List.exists (fun w -> red (reduce t w) u) (redexes t)
;
(* This definition works only for strongly normalisable terms, which do not
   have infinite reduction sequences. It tries to enumerate reducts of t in
   a depth-first manner, with redexes chosen in outer order. A more complete
   definition dove-tails the reductions in a breadth-first manner, giving
   a semi-decision algorithm for reduction, as follows: *)

(* All the one-step reducts of t - may contain duplicates *)
value reducts1 t = List.map (reduce t) (redexes t)
;
value red t u = redrec [t] (* breadth-first search *)
   where rec redrec = fun
    [ []     -> False 
    | [t::l] -> (t=u) || redrec(l @ (reducts1 t))
    ]
;
(*  red <<(!Omega (!Succ !Zero))>> <<(!Omega !One)>>;
    red <<(!Fix !K)>> <<(!K (!Fix !K))>>;
but red <<(!Y !K)>> <<(!K (!Y !K))>>; still loops *)

(* All the reducts of t - may not terminate *)
value rec reducts t = List.fold_left (fun r s -> r@(reducts s)) [t] (reducts1 t)
;
(* reducts _Omega; -- loops 
   reducts <<(!Pred' (!Succ' !Zero'))>>; returns a list of 120638 terms *)

(* We shall see later that reduction is actually an undecidable relation. *)

(* The following conversion relation is in the same spirit. It is based on the
confluence property of beta-reduction, which we shall prove later. It
enumerates all reductions issued from both terms, looking for a common reduct. *)

value common_reduct rewrite (t,u) = 
   let rec convrec1 = fun
    [ ([], rm, [], rn)      -> False
    | ([], rm, qn, rn)      -> convrec2([], rm, qn, rn)
    | ([m::qm], rm, qn, rn) -> List.mem m rn
                            || convrec2(qm@(rewrite m), [m::rm], qn, rn)
    ]
   and convrec2 = fun
    [ (qm, rm, [], rn)      -> convrec1(qm, rm, [], rn)
    | (qm, rm, [n::qn], rn) -> List.mem n rm
                            || convrec1(qm, rm, qn@(rewrite n), [n::rn])
    ] in
   convrec1 ([t],[],[u],[])
;
value conv t u = common_reduct reducts1 (t,u)
;
(* Whenever t and u are convertible (i.e. they have a common reduct), 
 (conv t u) will evaluate to True.
 When t and u are not convertible, (conv t u) may loop; it evaluates to False,
 when both t and u are strongly normalizing (all rewrite sequences terminate). *)
(* conv <<(!Y !K)>> <<(!K (!Y !K))>> = True; 
   conv <<(!Pred' (!Succ' !Zero'))>> _Zero' = True; (* Long computation! *)
   conv _Zero _Zero' = False; 
   conv _Y _Fix; -- loops *)

(* Exercice. Define versions reducts1 and reducts that compute sets of terms
(without repetitions). *)

(*******************************)
(* Leftmost-outermost strategy *)
(*******************************)

exception Normal_Form
;
value outermost = search_left root
  where rec search_left u = fun
    [ Ref _          -> raise Normal_Form
    | Abs e          -> search_left [F::u] e
    | App (Abs  _) _ -> List.rev u
    | App l r        -> try search_left [(A L)::u] l
                        with [ Normal_Form -> search_left [(A R)::u] r ]
    ]
;
(* The normal strategy *)

value one_step_normal t = reduce t (outermost t)
;
(* An equivalent optimised definition *)
value rec osn = fun
  [ Ref n         -> raise Normal_Form
  | Abs lam       -> Abs (osn lam)
  | App lam1 lam2 -> match lam1 with
     [ Abs lam   -> subst lam2 lam
     | _         -> try App (osn lam1) lam2
                    with [ Normal_Form -> App lam1 (osn lam2) ]
     ]
  ]
;

(* The normal strategy nf above may be seen as an optimisation of : *)

value rec normalise t = try normalise (osn t)
                        with [ Normal_Form -> t ]
;
(* let normal_nat t = compute_nat (normalise t) in 
   normal_nat<<(!Add !Two !Two)>> = 4; *)

(* The normal derivation sequence issued from a term *)
value rec normal_sequence t = 
     try let u = outermost t in
         let s = reduce t u in
         [u :: (normal_sequence s)]
     with [ Normal_Form -> [] ]
;
(*  List.length(normal_sequence <<(!Fact !Three)>>) = 191; *)

(* Comparing various strategies or interpreters. *)

(* The choice of the redex position argument of the reduce function
   being arbitrary, we have a non-deterministic computation rule. 
   However, we shall see that the computation is ultimately determinate,
   in the sense of confluence of beta-reduction. It is fairly easy to prove
   local confluence, in the form that for any positions u and v in e,
   we may get from the two terms (reduce e u) and (reduce e v) to a common
   one, reducing the sequence of (disjoint) (redex) positions
   respectively (residuals e u v) and (residuals e v u), as follows. *)


(*************)
(* Residuals *)
(*************)

(* All positions of the most recent free variable in a term *)
value locals = loc 0
where rec loc n = fun
 [ Ref m   -> if m=n then [root] else []
 | Abs t   -> List.map (cons F) (loc (n+1) t)
 | App t u -> List.map (cons (A L)) (loc n t) @ 
              List.map (cons (A R)) (loc n u)
 ]
;
(* All residuals of position v in term t after its reduction 
   at redex position u are given by (residuals t u v). This is a set
   of positions of the term (reduce t u). *)

value residuals t u =
  let x = locals(subterm(t,u@[A(L);F])) in fun v -> res(u,v)
  where rec res = fun
 [ ([],[])                   -> []
 | ([],[A L])                -> []
 | ([],[(A L)::[F::w]])      -> if List.mem w x then [] else [u@w]
 | ([],[(A R)::w])           -> let splice x = u@x@w in List.map splice x
 | ([],_)                    -> error "Position not in domain"
 | (_,[])                    -> [v]
 | ([F::w],[F::w'])          -> res(w,w')
 | ([(A s)::w],[(A s')::w']) -> if s=s' then res(w,w') else [v]
 | _                         -> error "Position not in domain"
 ]
;
(* Proposition. Let tr=(reduce t u).
  - If v is a redex position of t, then every position in (residuals t u v)
    is a redex position of tr.
  - For each position w of tr, there is exactly one position 
    v of t such that w is in (residuals t u v). *)
 

(* Definition. Let tr=(reduce t u). We say that v is a residual redex
   position in tr iff v is an position in tr which is a residual of
   some redex position in t. Other redex positions of tr are said to
   be created redex positions. *)

(* Exercice. Give all cases of creation of a redex. *)

(* Beware. The positions in (residuals t u v) are mutually disjoint,
and the corresponding sub-terms are equal (modulo lifting). But these
properties are not very important, because they are not preserved by
reduction, as the example below will demonstrate. *)

(* We could now proceed with proving local confluence of reduction.
However, this will not be directly sufficient, since the possible
non-termination of reduction does not allow using Newman's lemma to
get confluence from local confluence. We must rather prove a strong
confluence diagram, i.e the Diamond Lemma for a stronger notion of
parallel reduction. Confluence of beta-reduction will follow from the
fact that reduction and parallel reduction have the same reflexive-transitive
closure. *)

(* all_residuals by u of a set of redexes in t, represented as a list *)
value all_residuals t u = set_extension (residuals t u)
;
(* Now if we represent a derivation sequence seq as a list of redex 
   positions, we may trace the residuals of positions uu in 
   the term t as (trace_residuals (t,uu) seq). *)

value red_and_res (t,uu) v = (reduce t v, all_residuals t v uu)
;
value trace_residuals = List.fold_left red_and_res
;
(* v is a residual of pos in uu by reduction sequence seq issued from t *)
value is_residual t uu seq v = 
  List.mem v (snd (trace_residuals (t,uu) seq))
;
(* Important example : How a redex may have embedded residuals *)
(* (let t = <<[u]([z](z z) [y]([x]y u))>> 
    and seq = [[F]; [F]] 
    and r = [F; A R; F]    (* ________ *)
    in trace_residuals (t,[r]) seq) =
   (<<[u]([x,y]([w]y u) u)>>, [[F]; [F; A L; F; F]])
    (*   _________________     ^
                _______             ^  *)
*)

(* Thus, even though the residuals of one position after one step of
   reduction are disjoint, disjointness is not preserved by residuals,
   and thus we may have embedded residuals after several steps.
   Consequence: it is not enough to prove the diamond lemma with 
   parallel reduction of mutually disjoint redexes. We must define the
   simultaneous reduction of a set of redexes, possibly embedded. *)


