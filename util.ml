(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

value error message = raise (Failure message)
and fail () = raise (Failure "")
;
value even n = (n mod 2) = 0
and odd n  = (n mod 2) = 1
;
value rec iter f n x = if n=0 then x else iter f (n-1) (f x)
;
value cons x l = [ x :: l ]
;
value interval n p = 
let rec inter_rec l p = 
  let l'=[p::l] in 
  if p=n then l' else inter_rec l' (p-1) in
if n>p then [] else inter_rec [] p
;
value range n = interval 1 n
;
value set_extension f l = List.fold_right (fun u l -> (f u) @ l) l []
;

