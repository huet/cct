(**************************************************************************)
(*                                                                        *)
(*                          The Lambda Library                            *)
(*                                                                        *)
(*                              G�rard Huet                               *)
(*                                                                        *)
(* �2002 Institut National de Recherche en Informatique et en Automatique *)
(**************************************************************************)

open Util;
open Term;
open Reduction;


(******************)
(* Eta conversion *)
(******************)

(* An eta redex is a subterm of the form [x](f x), with x not appearing
free in f. Eta reduction consists in replacing this subterm by f.
In abstract form, we replace Abs (App (lift 1 t)) (Ref 0) by t. *)

value is_eta_redex t u = match subterm(t,u) with
 [ Abs (App f (Ref 0)) -> try let _ = unlift1 f in True
                          with [ Occurs_free -> False ]
 | _                   -> False
 ]
;
(* eta reduction *)
value eta_reduce t u = match subterm(t,u) with
  [ Abs (App f (Ref 0)) -> try replace (unlift1 f) (t,u)
                           with [ Occurs_free -> error "Not an eta redex" ]
  | _                   -> error "Not an eta redex"
  ]
;
(* Lemma. Eta reduction is confluent. *)

value eta_redexes t = List.filter (is_eta_redex t) (dom t)
;
value ered t = List.map (eta_reduce t) (eta_redexes t)
;
(* eta_conv is the equivalence closure of eta reduction. *)
value eta_conv = common_reduct ered
;
